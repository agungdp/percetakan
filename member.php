<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
auth(3, $config['member_akses']);
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-user fa-fw"></i> Member Area</h1>
				<div class="row">
					<div class="col-sm-3">
						<a href="./pesanan.php" class="thumbnail text-center" style="color: #333;">
							<i class="fa fa-shopping-cart fa-5x fa-fw"></i>
							<br><br>
							<button type="button" class="btn btn-primary btn-block">PESANAN</button>
						</a>
					</div>
					<div class="col-sm-3">
						<a href="./ubah_password.php" class="thumbnail text-center" style="color: orange;">
							<i class="fa fa-lock fa-5x fa-fw"></i>
							<br><br>
							<button type="button" class="btn btn-danger btn-block">UBAH PASSWORD</button>
						</a>
					</div>
					<div class="col-sm-3">
						<a href="./keluar.php" class="thumbnail text-center" style="color: red;">
							<i class="fa fa-sign-out fa-5x fa-fw"></i>
							<br><br>
							<button type="button" class="btn btn-danger btn-block">KELUAR</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
require_once './footer.php';