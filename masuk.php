<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
if(auth(2, $config['member_akses']) === true)	{
	redirect('./member.php');
}
if(input_post('masuk'))	{
	$email = trim(mysqli_real_escape_string($koneksi, input_post('email')));
	$password = trim(mysqli_real_escape_string($koneksi, input_post('password')));
	$akses = 'member';
	if(strlen($email) > 0 and strlen($password) > 0)	{
		$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where akses='{$akses}' and email='{$email}'"));
		if(count($user) > 0 and encryption(2, $password, $user['password']) === true)	{
			if($user['status'] == 'aktif')	{
				session_set('umasuk', true);
				session_set('uid', $user['id']);
				session_set('uakses', $akses);
				session_set('uemail', $user['email']);
				redirect('./member.php');
			}else{
				set_message('msg', 'danger', 'Akun Anda nonaktif.');
				redirect('./masuk.php');
			}
		}else{
			set_message('msg', 'danger', 'E-mail atau password salah.');
			redirect('./masuk.php');
		}
	}else{
		set_message('msg', 'danger', 'E-mail dan password harus diisi.');
		redirect('./masuk.php');
	}
}
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-sign-in fa-fw"></i> Masuk</h1>
				<div id="login-form">
					<?php echo get_message('msg'); ?>
					<form method="post" action="">
						<div class="form-group">
							<label for="email">E-mail</label>
							<input type="text" name="email" id="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" name="password" id="password" class="form-control">
						</div>
						<button type="submit" name="masuk" id="masuk" value="masuk" class="btn btn-default">
							<i class="fa fa-sign-in fa-fw"></i> Masuk
						</button>
					</form>
					<hr>
					<p>Belum jadi member?  <a href="./daftar.php">daftar</a> .</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
require_once './footer.php';