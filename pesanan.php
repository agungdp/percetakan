<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
auth(3, $config['member_akses']);
require_once './header.php';
require_once './navigasi.php';
// if (isset($_POST['report'])){
		// $laporan=($_POST['laporan']);
		// $laporan1=substr($laporan,0,10);
		// $laporan2=substr($laporan,12,22);
		// echo $laporan1."<br />";
		// echo $laporan2;
// }

?>

<html>
	<head>
		<title>
			Pesanan
		</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <style media="screen">
    
      #message{
        display: none;
      }
    </style>
	<script src="https://code.jquery.com/jquery-1.12.4.js">

</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js">

</script>
<script>
	$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'print'
        ]
    } );
} );
</script>
<link rel="stylesheet" href="css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="css/buttons.dataTables.min.css" />

<!-- Include Required Prerequisites -->

<script type="text/javascript" src="js/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	</head>	
	<body>	
<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-shopping-cart fa-fw"></i> Pesanan</h1>
					<?php
						if(isset ($_SESSION['uemail'])){
						$a=$_SESSION['uemail'];
						$query = $koneksi ->query("SELECT id,nama from user  where email LIKE'".$a."%'");
						 if($data=mysqli_fetch_array($query)){
					?>
					<form action="mpdf/report.php" method="post">
						<table id="table">
							<tr>
								<td>
									<input type="hidden"  name="id" value="<?php echo strtoupper($data['id'])?>"/> 
								</td>
								<td>
									Pilih Tanggal
								</td>
								
								<td>	
									:
								</td>
								
								<td>
									&nbsp;&nbsp;<input type="text" id="daterange" name="laporan" /> 
								</td>
								
								<td>
									&nbsp;&nbsp;<select name="pilihan_hidup">
										<?php
											$array=array("Pilih","proses","Di Bayar","Dikirim","Selesai");
											foreach ($array as $b){
												echo "<option>".$b."</option>";
											}
										?>
									</select>
								</td>
								<td>
									&nbsp;&nbsp;<input type="submit" name="report" value="DOWNLOAD" class="btn btn-info" />
								</td>
							</tr>
						</table>
					</form>
					<?php
						
						}else{
							echo "silahkan login";
						}
						}
			
					?>

				<?php echo get_message('msg'); ?>
				<div>
					<table id="example" class="display nowrap" width="100%" cellspacing="0">
						<thead>
							<tr>
									<th>id</th>
										<th> id_user</th>
								<th>Kode pesanan</th>
								<th class="text-center">Waktu pesanan</th>
								<th class="text-center">Waktu Selesai</th>
								<th>Paket</th>
								<th class="text-right">Jumlah halaman</th>
								<th class="text-right">Harga dasar</th>
								<th class="text-right">Harga per halaman</th>
								<th class="text-right">Total</th>
								<th>Status</th>
							</tr>
						</thead>
						
						<tbody>
							<?php
							$pesanan_query = mysqli_query($koneksi, "select up.*,p.nama as paket from user_paket up join paket p on up.id_paket=p.id where up.id_user=".session_get('uid'));
							while($pesanan = mysqli_fetch_assoc($pesanan_query))	{
								echo '<tr>';
								echo '<td class="text-center">'.$pesanan['id_user_paket'].'</td>';
								echo '<td> '.$pesanan['id_user'].'</td>';
								echo '<td><a href="./pesanan.php?mode=view&id='.$pesanan['kode_pesanan'].'">'.$pesanan['kode_pesanan'].'</a></td>';
								echo '<td class="text-center">'.$pesanan['waktu_pemesanan'].'</td>';
								echo '<td class="text-center">'.$pesanan['waktu_selesai'].'</td>';
								echo '<td>'.$pesanan['paket'].'</td>';
								echo '<td class="text-right">'.$pesanan['jumlah_halaman'].'</td>';
								echo '<td class="text-right">'.rupiah($pesanan['harga_dasar']).'</td>';
								echo '<td class="text-right">'.rupiah($pesanan['harga_halaman']).'</td>';
								echo '<td class="text-right">'.rupiah($pesanan['total_harga']).'</td>';
								echo '<td>'.pesanan_status(2, $pesanan['status']).'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
<script type="text/javascript">
		$(function() {
			$('#daterange').daterangepicker({
				locale: {
				  format: 'YYYY-MM-DD'
			},
   
			}, 
					function(start, end, label) {
						alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
				});
			});
		</script>
</html>


<?php
require_once './footer.php';
