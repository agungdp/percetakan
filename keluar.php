<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
auth(3, $config['member_akses']);
$mode = input_get('mode');
$user_id = session_get('uid');
unset_session('umasuk');
unset_session('uid');
unset_session('uakses');
unset_session('uemail');
if($mode == 'ubah_password')	{
	set_message('msg', 'warning', 'Anda baru saja mengubah password.');
}else{
	set_message('msg', 'info', 'Anda baru saja keluar dari sistem.');
}
redirect('./masuk.php');