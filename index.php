<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div id="carousel1" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#carousel1" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel1" data-slide-to="1"></li>
                    <li data-target="#carousel1" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="assets/slides/slider1.jpg" alt="One" style="width: 100%;">
                      <div class="carousel-caption"></div>
                    </div>
                    <div class="item">
                      <img src="assets/slides/slider2.jpg" alt="Two" style="width: 100%;">
                      <div class="carousel-caption"></div>
                    </div>
                    <div class="item">
                      <img src="assets/slides/slider3.jpg" alt="Two" style="width: 100%;">
                      <div class="carousel-caption"></div>
                    </div>
                  </div>

                  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                <hr>
                <div>
                	<h1>ELPUEBLO</h1>
                	<p>Percetakan ELPUEBLO terletak di  Jl Selokan Mataram, Cepit Baru Rt. 10 Rw. 38 Condong Catur, Depok, Sleman, Yogyakarta menerima pembuatan buku, kalender, banner dan lain-lain. .</p>
                </div>
                <div class="row">
                	<div class="col-sm-4">
                		<div class="panel panel-default">
                			<div class="panel-heading">CETAK BUKU</div>
                			<div class="panel-body">
                				<p>Hasil cetakan yang bagus, printer berkualitas</p>
                				<p><a href="./paket.php" class="btn btn-primary btn-block">Pilih yang ini</a></p>
                			</div>
                		</div>
                	</div>
                	
                		</div>
                	</div>
                </div>
            </div>
            <div class="col-sm-4 hidden">
            	<?php
            	$paket_query = mysqli_query($koneksi, "select * from paket order by nama asc");
            	while($paket = mysqli_fetch_assoc($paket_query))	{
            		?>
            		<div class="panel panel-primary">
            			<div class="panel-heading"><?php echo $paket['nama']; ?></div>
            			<div class="panel-body">
            				<?php
            				if(strlen($paket['gambar']) > 0)	{
            					?>
            					<div>
            						<img src="assets/upload/<?php echo $paket['gambar']; ?>" class="img-responsive thumbnail">
            					</div>
            					<?php
            				}
            				if(strlen($paket['keterangan']) > 0)	{
            					?>
            					<p><?php echo substr(strip_tags($paket['keterangan']), 0, 150); ?></p>
            					<?php
            				}
            				?>
            				<p>Harga dasar: <strong><?php echo rupiah($paket['harga_dasar']); ?></strong> | Harga per halaman: <strong><?php echo rupiah($paket['harga_halaman']); ?></strong> | Jumlah halaman minimal: <strong><?php echo $paket['min_halaman']; ?></strong> | Jumlah halaman maksimal: <strong><?php echo $paket['max_halaman']; ?></strong></p>
            				<p><a href="./pesan_paket.php?id=<?php echo $paket['id']; ?>" class="btn btn-success btn-block"><i class="fa fa-check fa-fw"></i> Pesan sekarang</a></p>
            			</div>
            		</div>
            		<?php
            	}
            	?>
            </div>
        </div>
    </div>
</div>
<?php
require_once './footer.php';