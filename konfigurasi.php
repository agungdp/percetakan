<?php
defined('PASSED') or exit('No direct script access allowed!');
$config['base_url'] = 'http://localhost/percetakan/';
$config['admin_akses'] = array('administrator', 'operator');
$config['member_akses'] = array('member');
$config['hidden_users'] = array(1, 2);