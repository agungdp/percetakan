-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2018 at 11:10 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `percetakan`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(150) NOT NULL,
  `isi` text,
  `id_admin` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi`
--

CREATE TABLE `konfirmasi` (
  `id` int(20) UNSIGNED NOT NULL,
  `id_user` int(20) UNSIGNED NOT NULL,
  `kode_pesanan` varchar(100) NOT NULL,
  `id_transaksi` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(25) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `bank_asal` varchar(25) NOT NULL,
  `bank_tujuan` varchar(25) DEFAULT NULL,
  `lampiran` varchar(50) NOT NULL,
  `rekening_asal` varchar(25) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `jumlah` double(13,2) DEFAULT '0.00',
  `waktu` datetime DEFAULT NULL,
  `deadline` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasi`
--

INSERT INTO `konfirmasi` (`id`, `id_user`, `kode_pesanan`, `id_transaksi`, `nama`, `alamat`, `email`, `bank_asal`, `bank_tujuan`, `lampiran`, `rekening_asal`, `gambar`, `jumlah`, `waktu`, `deadline`) VALUES
(91, 11, '8845-13-2054', 13, 'hhahah', 'palembang', 'agung.praseptyo@gmail.com', 'BCA', 'BRI', 'ada', '4444', 'doc.pdf', 56666.00, '2018-01-31 00:00:00', '2018-02-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id` int(5) UNSIGNED NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `harga_dasar` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `harga_halaman` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `min_halaman` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `max_halaman` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id`, `gambar`, `nama`, `harga_dasar`, `harga_halaman`, `min_halaman`, `max_halaman`, `keterangan`) VALUES
(4, 'img027.jpg', 'Paket A', 35000, 500, 10, 1000, '<p>Tes 1000</p>'),
(5, '20141209_181206.jpg', 'Paket B', 40000, 650, 20, 200, ''),
(6, '', 'kalender', 0, 2000, 12, 1200, '<p>xxxxxxx</p>'),
(7, '', 'sassad', 24455, 234555, 2345, 456, '<p>hahaa</p>'),
(8, 'a.PNG', 'rumah agung', 2345, 2344, 123, 123, '<p>sdsad</p>'),
(9, '', 'indra', 1234, 23455, 2344, 2344, '<p>aaaa</p>'),
(10, '', 'agungg', 3455, 45566, 34555, 34555, '<p>aaa</p>'),
(11, '', 'ini', 12344, 1223333, 123333, 123, '<p>aaaa</p>'),
(12, '', 'dhdshsda', 12344, 23, 355, 57, '<p>ahah</p>'),
(13, '', 'sdasasd', 12444, 2323231, 12323231, 2123213, ''),
(15, '', 'assda', 124444, 234444, 555555, 3333, '<p>assdasd</p>'),
(16, 'Desert.jpg', 'anwar', 123444, 28, 6, 3, '<p>hallo dek</p>'),
(17, 'a.PNG', 'ibnu', 234444, 3455, 4444, 4555, '<p>aaa</p>'),
(18, 'Desert.jpg', 'chriski', 1233, 45566, 5555, 7777, '<p>ssdds</p>'),
(19, '', 'inri', 123344, 45555, 5555, 5555, '<p>gggg</p>');

-- --------------------------------------------------------

--
-- Table structure for table `range_waktu`
--

CREATE TABLE `range_waktu` (
  `id` int(11) NOT NULL,
  `jmldate` varchar(1000) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `range_waktu`
--

INSERT INTO `range_waktu` (`id`, `jmldate`, `waktu`) VALUES
(1, '12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `akses` enum('administrator','operator','member') NOT NULL DEFAULT 'member',
  `kode_registrasi` varchar(40) DEFAULT NULL,
  `status` enum('aktif','nonaktif') NOT NULL DEFAULT 'aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `alamat`, `telepon`, `email`, `password`, `akses`, `kode_registrasi`, `status`) VALUES
(0, 'Administrator', '', '', 'admin', '$2y$11$lc4YtKBKamFeT5Cb5TrzOOMuXi7b9ZQIlRA8nXmBo36i78VhbSNKC', 'administrator', NULL, 'aktif'),
(1, 'System', '', '', 'system', '$2y$11$LjGtEX2flnbrgTJThd3dKunGokQTat8bNnKiMj7Gp0qrE2O5SjlJK', 'administrator', NULL, 'aktif'),
(3, 'Janu Dewangga', 'Jl. Semin - Bendung Km. 1', '087738210673', 'janu.dewangga@gmail.com', '$2y$11$r7Lf3ecrPOxTQuve4dJcveHr48o4nFZb4Ix.4YLwMISvVFz7j.px6', 'administrator', NULL, 'aktif'),
(5, 'Member 1', 'Alamat member 1', '088171615278', 'member1@percetakan.ody', '$2y$11$85dRI3v7iyFQR6rksz.se.d47KPhdda7awHtRYWH30OHy7M1PcUMu', 'member', NULL, 'aktif'),
(6, 'Ahmad Mustofa', 'Jl. Kiai Geni 212, Yogyakarta', '085671627166', 'ahmad@kiai212.com', '$2y$11$uzauv34Wx6h5FHVaENn/lOcvRwXT6pGXeO7obVHFb64RO/IqFiurK', 'member', NULL, 'aktif'),
(7, 'Firman Nagapasha', 'Klentingan No. 5', '087786156171', 'firmannaga@pash.com', '$2y$11$uNyEqpwXaILUZMywSjB9QuU43AUgRx/XaHDuoHF2WDJd1/.dXR3IS', 'member', NULL, 'aktif'),
(8, 'ody permana', 'jl glagahsari no 78 yogyakarta', '085741078869', 'odipermanaputra@gmail.com', '$2y$11$Dz0DM9ON8VngLF/gik2R3OCK2VRVYV5ZA2fW7J7mM6Gh763Mb3z2O', 'member', 'c456f70aff6016d589679909696bd621d1f25bdf', 'aktif'),
(9, 'permana putra', 'jl glagahsari no 87 yogyakarta', '08572888869', 'permanaputra@gmail.com', '$2y$11$s7Zz83HiSCOLHUxiEMazMeBKwYyHIz4lVebbCiEBuhOXsXEDgat3C', 'administrator', '929036c555ae6e4b988f08fd8799932738657994', 'aktif'),
(10, 'ody permana', 'jl glagahsari no 87', '085741078869', 'odypermana@gmail.com', '$2y$11$w7LsBMcHnEOxQg0eaXPwceA7sv37wqxFcRf/LFiep0nrFMl2o.M3q', 'administrator', '7246c366c3d3309c013317270ec27a173d5aa927', 'aktif'),
(11, 'dinda', 'bantul', '12345', 'dinda@gmail.com', '$2y$11$vm8loqvgwm3vBeD8/DdZUO8wL1DBySLtqhb1IcTRzspaIyOUqpaq.', 'member', 'b935db0e6744dee2b65b5bea766c5dc6a1421368', 'aktif'),
(12, 'karya indonesia', 'adsds', 'sdasad', 'agung.praseptyo@gmail.com', '$2y$11$1Tx0iFIpDz9iHQTNmAJX9Oorro4bS5.rCctDIU7ivxIwm./7OzQ.6', 'member', '0493740c51e5f5784bafbe22731ffce144d2d8e4', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `user_paket`
--

CREATE TABLE `user_paket` (
  `id_user_paket` int(10) UNSIGNED NOT NULL,
  `kode_pesanan` varchar(100) DEFAULT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_paket` int(5) UNSIGNED NOT NULL,
  `jumlah_halaman` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `harga_dasar` double(13,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `harga_halaman` double(13,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `total_harga` double(13,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `keterangan` text,
  `waktu_pemesanan` datetime DEFAULT NULL,
  `waktu_selesai` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_admin` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` enum('proses','dibayar','dikirim','selesai') NOT NULL DEFAULT 'proses'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_paket`
--

INSERT INTO `user_paket` (`id_user_paket`, `kode_pesanan`, `id_user`, `id_paket`, `jumlah_halaman`, `harga_dasar`, `harga_halaman`, `total_harga`, `keterangan`, `waktu_pemesanan`, `waktu_selesai`, `id_admin`, `status`) VALUES
(16, '7484-16-6510', 12, 16, 1233, 123444.00, 28.00, 157968.00, 'haha', '2018-01-20 00:12:19', '2018-01-23 00:19:12', 0, 'proses'),
(17, '3136-17-4813', 12, 16, 3445, 123444.00, 28.00, 219904.00, 'coba', '2018-01-20 00:13:16', '2018-01-23 00:16:13', 0, 'proses'),
(18, '4493-18-4118', 12, 10, 1223, 3455.00, 45566.00, 55730673.00, 'buku bagus', '2018-01-20 00:25:03', '2018-01-23 00:03:25', 0, 'proses'),
(19, '7260-19-6130', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 00:40:02', '1970-01-01 07:00:00', 0, 'proses'),
(20, '3547-20-5851', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 00:44:17', '1970-01-01 07:00:00', 0, 'proses'),
(21, '6180-21-8584', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 00:44:37', '1970-01-01 07:00:00', 0, 'proses'),
(22, '6451-22-2672', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 00:44:46', '1970-01-01 07:00:00', 0, 'proses'),
(23, '3037-23-1148', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 00:45:01', '1970-01-01 07:00:00', 0, 'proses'),
(24, '4261-24-3105', 12, 16, 2344, 123444.00, 28.00, 189076.00, 'saadah', '2018-01-20 00:45:17', '1970-01-01 07:00:00', 0, 'proses'),
(25, '1563-25-3541', 12, 16, 2344, 123444.00, 28.00, 189076.00, 'saadah', '2018-01-20 00:45:56', '1970-01-01 07:00:00', 0, 'proses'),
(26, '2160-26-5514', 12, 16, 2344, 123444.00, 28.00, 189076.00, 'saadah', '2018-01-20 00:46:18', '1970-01-01 07:00:00', 0, 'proses'),
(27, '7386-27-2128', 12, 4, 1233, 35000.00, 500.00, 651500.00, 'adadadada', '2018-01-20 11:47:33', '1970-01-01 07:00:00', 0, 'proses'),
(28, '4128-28-8814', 12, 4, 1233, 35000.00, 500.00, 651500.00, 'testong', '2018-01-20 11:54:04', '1970-01-01 07:00:00', 0, 'proses'),
(29, '1883-29-8089', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'agunggggg', '2018-01-20 15:07:40', '1970-01-01 07:00:00', 0, 'proses'),
(30, '2814-30-5151', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:09:43', '1970-01-01 07:00:00', 0, 'proses'),
(31, '9807-31-6482', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 00:00:00', '1970-01-01 07:00:00', 0, 'proses'),
(32, '5295-32-9634', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:25:58', '1970-01-01 07:00:00', 0, 'proses'),
(33, '6477-33-9389', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:26:07', '0000-00-00 00:00:00', 0, 'proses'),
(34, '5054-34-7554', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:26:46', '1970-01-01 07:00:00', 0, 'proses'),
(35, '6838-35-4799', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:27:13', '2018-01-23 15:13:27', 0, 'proses'),
(36, '2308-36-6706', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:27:32', '1970-01-01 07:00:00', 0, 'proses'),
(37, '6621-37-2957', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:28:03', '2018-01-23 15:03:28', 0, 'proses'),
(38, '8933-38-4141', 12, 10, 1233, 3455.00, 45566.00, 56186333.00, 'desert', '2018-01-20 15:29:24', '2018-01-24 15:24:29', 0, 'proses'),
(39, '8372-39-8272', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:05:32', '2018-02-04 17:32:05', 0, 'proses'),
(40, '2293-40-1334', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:06:20', '2018-01-24 17:20:06', 0, 'proses'),
(41, '7795-41-4350', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:06:37', '2018-01-21 17:37:06', 0, 'proses'),
(42, '1713-42-7807', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:06:50', '2018-02-01 17:50:06', 0, 'proses'),
(43, '6692-43-8452', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:07:28', '2018-01-25 17:28:07', 0, 'proses'),
(44, '7047-44-2823', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:07:36', '2018-02-04 17:36:07', 0, 'proses'),
(45, '7238-45-3349', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:07:58', '2018-02-04 17:58:07', 0, 'proses'),
(46, '8093-46-7803', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:08:06', '2018-01-26 17:06:08', 0, 'proses'),
(47, '5938-47-1168', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:08:13', '2018-01-28 17:13:08', 0, 'proses'),
(48, '8635-48-2490', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:08:21', '2018-01-29 17:21:08', 0, 'proses'),
(49, '1377-49-5344', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:08:27', '2018-01-30 17:27:08', 0, 'proses'),
(50, '5445-50-9409', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:08:42', '2018-02-04 17:42:08', 0, 'proses'),
(51, '7490-51-6531', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:09:10', '2018-02-01 17:10:09', 0, 'proses'),
(52, '2627-52-1583', 12, 10, 2344, 3455.00, 45566.00, 106810159.00, '2456', '2018-01-20 17:09:36', '2018-02-01 17:36:09', 0, 'proses');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `judul` (`judul`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `konfirmasi`
--
ALTER TABLE `konfirmasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `range_waktu`
--
ALTER TABLE `range_waktu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_paket`
--
ALTER TABLE `user_paket`
  ADD PRIMARY KEY (`id_user_paket`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_paket` (`id_paket`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `konfirmasi`
--
ALTER TABLE `konfirmasi`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `range_waktu`
--
ALTER TABLE `range_waktu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_paket`
--
ALTER TABLE `user_paket`
  MODIFY `id_user_paket` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `user` (`id`);

--
-- Constraints for table `user_paket`
--
ALTER TABLE `user_paket`
  ADD CONSTRAINT `user_paket_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `user_paket_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
