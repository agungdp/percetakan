<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
$form_error = null;
// auth(3, $config['member_akses']);
$mode = input_get('mode');
if($mode == 'pesan')	{
	auth(3, $config['member_akses']);
	$id = input_get('id');
	$paket = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from paket where id=$id"));
	if(input_post('ok'))	{
		$jml_halaman = input_post('jml_halaman');
		$keterangan = mysqli_real_escape_string($koneksi, input_post('keterangan'));
		$fval = true;
		if(strlen($jml_halaman) < 1)	{
			$fval = false;
			$form_error['jml_halaman'] = error_generator(1, 'Jumlah halaman harus diisi.');
		}else{
			if(filter_var($jml_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['jml_halaman'] = error_generator(1, 'Jumlah halaman hanya boleh diisi bilangan bulat.');
			}
		}
		if($fval === true)	{
			$upl = upload_file('dokumen', array('doc','jpg','docx','xls','xlsx','ppt','pptx','pdf'), './assets/upload/');
			if($upl !== false)	{
				$read = mysqli_query($koneksi,"SELECT jmldate FROM range_waktu limit 1");
				  if($data=mysqli_fetch_array($read)){
					   $jmlday =$data['jmldate'];
				  }else{
					  echo "Tidak ada data range";
				  }
				$waktu = date("Y-m-d H:i:s", time());
				//$date=explode("-",$waktu);
				  list($m, $d, $y) = explode ( '/', $waktu);
				  $detik = date("H:i:s");
				  $tanggal = $y."-".$m."-".$d."&nbsp;".$detik;
				  
				  $tanggal1 = date( 'Y-m-d H:s:i',strtotime("+".$jmlday."days", strtotime($waktu)));
				  echo "<br />".$tanggal1;
				 //echo $tanggal=date("Y-m-d H:i:s");
				$total_harga = $paket['harga_dasar'] + ($jml_halaman * $paket['harga_halaman']);				
				mysqli_query($koneksi, "insert into user_paket(id_user, id_paket, jumlah_halaman, harga_dasar, harga_halaman, total_harga, keterangan, waktu_pemesanan,waktu_selesai) values(".session_get('uid').",$id,$jml_halaman, $paket[harga_dasar], $paket[harga_halaman],$total_harga,'$keterangan','$waktu','$tanggal1')");
				$pesanan = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user_paket order by id_user_paket desc limit 1"));
				$kode_pesanan = rand(1111, 9999).'-'.$pesanan['id_user_paket'].'-'.rand(1111, 9999);
				mysqli_query($koneksi, "update user_paket set kode_pesanan='{$kode_pesanan}' where id_user_paket=$pesanan[id_user_paket]");
				set_message('msg', 'success', 'Terimakasih, pesanan Anda telah kami terima dan segera kami proses. Selajnutnya kami akan mengirimkan notifikasi via e-mail, dan Anda dapat melihat status pesanan Anda di halaman member.');
				redirect('./pesanan.php');
			}else{
				$form_error['dokumen'] = error_generator(1, 'Dokumen yang Anda upload tidak sesuai dengan ketentuan.');
			}
		}
	}
}
require_once './header.php';
require_once './navigasi.php';
if($mode == 'pesan')	{
	?>
	<div class="body-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-wpforms fa-fw"></i> Paket skripsi | <?php echo $paket['nama']; ?></h1>
					<div class="row">
						<div class="col-sm-4">
							<img src="assets/upload/<?php echo $paket['gambar']; ?>" style="width: 100px; float: left; margin: 0 10px 10px 0;">
							<?php
								if(strlen($paket['keterangan']) > 0)	{
									echo $paket['keterangan'];
								}
							?>
							<hr>
							<div>Harga dasar: <strong><?php echo rupiah($paket['harga_dasar']); ?></strong> | Harga per halaman isi: <strong><?php echo rupiah($paket['harga_halaman']); ?></strong> | Jumlah minimal halaman: <strong><?php echo $paket['min_halaman']; ?></strong> | Jumlah maksimal halaman: <strong><?php echo $paket['max_halaman']; ?></strong></div>
						</div>
						<div class="col-sm-8">
							<form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
								<div class="form-group">
									<label for="dokumen" class="control-label col-sm-3">File/dokumen</label>
									<div class="col-sm-9">
										<input type="file" name="dokumen" id="dokumen">
										<?php echo show_error(1, $form_error, 'dokumen'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="jml_halaman" class="control-label col-sm-3">Jumlah halaman/isi</label>
									<div class="col-sm-5">
										<input type="text" name="jml_halaman" id="jml_halaman" class="form-control" value="<?php echo form_set_value('jml_halaman'); ?>" maxlength="4">
										<?php echo show_error(1, $form_error, 'jml_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="keterangan" class="control-label col-sm-3">Keterangan</label>
									<div class="col-sm-9">
										<textarea name="keterangan" id="keterangan" class="form-control" rows="5"><?php echo form_set_value('keterangan'); ?></textarea>
										<?php echo show_error(1, $form_error, 'keterangan'); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" name="ok" value="ok" id="ok" class="btn btn-primary"><i class="fa fa-check fa-fw"></i> Selanjutnya</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}else{
	?>
	<div class="body-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h1 class="title1"><i class="fa fa-wpforms fa-fw"></i> Paket skripsi</h1>
					<?php
					$paket_query = mysqli_query($koneksi, "select * from paket order by nama asc");
					while($paket = mysqli_fetch_assoc($paket_query))	{
						?>
						<div class="row">
							<div class="col-sm-2">
								<img src="assets/upload/<?php echo $paket['gambar']; ?>" class="img-responsive">
							</div>
							<div class="col-sm-10">
								<h2 style="margin-top: 0;"><?php echo $paket['nama']; ?></h2>
								<?php
									if(strlen($paket['keterangan']) > 0)	{
										echo $paket['keterangan'];
									}
								?>
								<hr>
								<div>Harga dasar: <strong><?php echo rupiah($paket['harga_dasar']); ?></strong> | Harga per halaman isi: <strong><?php echo rupiah($paket['harga_halaman']); ?></strong> | Jumlah minimal halaman: <strong><?php echo $paket['min_halaman']; ?></strong> | Jumlah maksimal halaman: <strong><?php echo $paket['max_halaman']; ?></strong></div>
								<div><a class="btn btn-primary" href="./paket.php?mode=pesan&id=<?php echo $paket['id']; ?>"><i class="fa fa-shopping-cart fa-fw"></i> Pesan sekarang</a></div>
							</div>
						</div><br>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>
<?php
require_once './footer.php';
?>