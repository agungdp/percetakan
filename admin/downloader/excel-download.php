<?php
include ("../../koneksi/connect.php");
/** Set default timezone (will throw a notice otherwise) */
$datak=$d->query("SELECT * FROM data_lelco");
date_default_timezone_set('Asia/Jakarta');

// include PHPExcel
require('../PHPExcel.php');

// create new PHPExcel object
$objPHPExcel = new PHPExcel;

// set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

// set default font size
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

// create the writer
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

 

/**

 * Define currency and number format.

 */

// currency format, € with < 0 being in red color
$currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

// number format, with thousands separator and two decimal points.
$numberFormat = '#,#0.##;[Red]-#,#0.##';

 

// writer already created the first sheet for us, let's get it
$objSheet = $objPHPExcel->getActiveSheet();

// rename the sheet
$objSheet->setTitle('LELCO MANUFACTURING');

 

// let's bold and size the header font and write the header
// as you can see, we can specify a range of cells, like here: cells from A1 to A4
$objSheet->getStyle('A1:D1')->getFont()->setBold(true)->setSize(12);

 

// write header

// Set document properties
$objPHPExcel->getProperties()->setCreator("ADMIN LELCO MANUFACTURING")
							->setLastModifiedBy("ADMIN LELCO MANUFACTURING")
							->setTitle("Data Siswa")
							->setSubject("Siswa")
							->setDescription("Data siswa tahun ajaran 2015/2016")
							->setKeywords("sibangStudio PHPExcel php")
							->setCategory("Umum");
$objDrawing = new PHPExcel_Worksheet_Drawing();

$objDrawing->setPath('../../gambar/logo4.png');
$objDrawing->setCoordinates('B2');
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
// mulai dari baris ke 2
$row = 2;
 
// Tulis judul tabel
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1'.$row, 'Nomor Urut')
            ->setCellValue('B1'.$row, 'No WO')
            ->setCellValue('C1'.$row, 'qty')
            ->setCellValue('D1'.$row, 'shift');

 
$nomor 	= 1; // set nomor urut = 1;
 
$row++; // pindah ke row bawahnya. (ke row 2)
 
// lakukan perulangan untuk menuliskan data siswa
while( $data = mysqli_fetch_array($datak)){
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1'.$row,  $nomor )
            ->setCellValue('B1'.$row, $data['wo_code'] )
            ->setCellValue('C1'.$row, $data['qty'] )
            ->setCellValue('D1'.$row, $data['shift'] );

			
	$row++; // pindah ke row bawahnya ($row + 1)
	$nomor++;
}
// autosize the columns
$objSheet->getColumnDimension('A')->setAutoSize(true);
$objSheet->getColumnDimension('B')->setAutoSize(true);
$objSheet->getColumnDimension('C')->setAutoSize(true);
$objSheet->getColumnDimension('D')->setAutoSize(true);


//Setting the header type
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="LELCOMANUFACTURING.xlsx"');
header('Cache-Control: max-age=0');

$objWriter->save('php://output');

/* If you want to save the file on the server instead of downloading, replace the last 4 lines by 
	$objWriter->save('test.xlsx');
*/

?>
