<?php
defined('PASSED') or exit('No direct script access allowed!');
$nav1 = '<li><a href="./user.php"><i class="fa fa-group fa-fw"></i> User</a></li>';
?>
<nav class="navbar navbar-default navbar-fixed-top" style="border-bottom: 1px solid #ccc;">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./index.php"><i class="fa fa-dashboard fa-fw"></i></a>
		</div>
		<div class="collapse navbar-collapse" id="top-nav">
			<ul class="nav navbar-nav">
				<?php echo auth(4, 'administrator', $nav1); ?>
				<li><a href="./paket.php"><i class="fa fa-file-archive-o fa-fw"></i> Paket cetak</a></li>
				<li><a href="./pesanan.php"><i class="fa fa-list-alt fa-fw"></i> Pesanan</a></li>
				<li><a href="./hasil.php"><i class="fa fa-list-alt fa-fw"></i> Konfirmasi</a></li>
				<li><a href="./range.php"><i class="fa fa-list-alt fa-fw"></i> DATE RANGE</a></li>
				
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-fw"></i> <?php echo session_get('uemail'); ?> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="./ubah_password.php"><i class="fa fa-lock fa-fw"></i> Ubah password</a></li>
						<li><a href="./keluar.php"><i class="fa fa-sign-out fa-fw"></i> Keluar</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>