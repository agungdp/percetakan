<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
auth(3, 'administrator');
$form_error = null;
$mode = input_get('mode');
if($mode == 'tambah')	{
	if(input_post('ok'))	{
		$akses = input_post('akses');
		$nama = input_post('nama');
		$alamat = input_post('alamat');
		$telepon = input_post('telepon');
		$email = trim(input_post('email'));
		$password = trim(input_post('password'));
		$konfirmasi = trim(input_post('konfirmasi'));
		$fval = true;
		if(strlen($nama) < 1)	{
			$fval = false;
			$form_error['nama'] = error_generator(1, 'Nama harus diisi.');
		}
		if(strlen($alamat) < 1)	{
			$fval = false;
			$form_error['alamat'] = error_generator(1, 'Alamat harus diisi.');
		}
		if(strlen($telepon) < 1)	{
			$fval = false;
			$form_error['telepon'] = error_generator(1, 'Telepon harus diisi.');
		}
		if(strlen($email) < 4)	{
			$fval = false;
			$form_error['email'] = error_generator(1, 'E-mail harus diisi, minimal 4 karakter.');
		}else{
			if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)	{
				$fval = false;
				$form_error['email'] = error_generator(1, 'Format e-mail salah. Silahkan periksa kembali.');
			}else{
				if(mysqli_num_rows(mysqli_query($koneksi, "select id from user where email='{$email}'")) > 0)	{
					$fval = false;
					$form_error['email'] = error_generator(1, 'E-mail tersebut telah digunakan.');
				}
			}
		}
		if(strlen($password) < 8)	{
			$fval = false;
			$form_error['password'] = error_generator(1, 'Password harus diisi, minimal 8 karakter.');
		}else{
			if($konfirmasi !== $password)	{
				$fval = false;
				$form_error['konfirmasi'] = error_generator(1, 'Konfirmasi harus sama dengan password.');
			}
		}
		if($fval === true)	{
			$pass = encryption(1, $password);
			mysqli_query($koneksi, "insert into user(nama, alamat, telepon, email, password, akses) values('{$nama}','{$alamat}','{$telepon}','{$email}','{$pass}','{$akses}')");
			$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where email='{$email}'"));
			set_message('msg', 'success', "Anda baru saja menambah user baru: <strong>{$user['nama']} (e-mail: {$user['email']})</strong>.");
			redirect('./user.php');
		}
	}
}elseif($mode == 'edit')	{
	$id = input_get('id');
	if(in_array($id, $config['hidden_users']))	{
		set_message('msg', 'danger', 'Anda tidak diperbolehkan mengedit user tersebut.');
		redirect('./user.php');
	}else{
		$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where id={$id}"));
		if(input_post('ok'))	{
			$akses = input_post('akses');
			$nama = input_post('nama');
			$alamat = input_post('alamat');
			$telepon = input_post('telepon');
			$email = trim(input_post('email'));
			$password = trim(input_post('password'));
			$konfirmasi = trim(input_post('konfirmasi'));
			$status = input_post('status');
			$fval = true;
			if(strlen($nama) < 1)	{
				$fval = false;
				$form_error['nama'] = error_generator(1, 'Nama harus diisi.');
			}
			if(strlen($alamat) < 1)	{
				$fval = false;
				$form_error['alamat'] = error_generator(1, 'Alamat harus diisi.');
			}
			if(strlen($telepon) < 1)	{
				$fval = false;
				$form_error['telepon'] = error_generator(1, 'Telepon harus diisi.');
			}
			if(strlen($email) < 4)	{
				$fval = false;
				$form_error['email'] = error_generator(1, 'E-mail harus diisi, minimal 4 karakter.');
			}else{
				if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)	{
					$fval = false;
					$form_error['email'] = error_generator(1, 'Format e-mail salah. Silahkan periksa kembali.');
				}else{
					if(mysqli_num_rows(mysqli_query($koneksi, "select id from user where email='{$email}' and id!={$id}")) > 0)	{
						$fval = false;
						$form_error['email'] = error_generator(1, 'E-mail tersebut telah digunakan.');
					}
				}
			}
			if(strlen($password) > 0)	{
				$cpass = true;
				if(strlen($password) < 8)	{
					$fval = false;
					$form_error['password'] = error_generator(1, 'Password harus diisi, minimal 8 karakter.');
				}else{
					if($konfirmasi !== $password)	{
						$fval = false;
						$form_error['konfirmasi'] = error_generator(1, 'Konfirmasi harus sama dengan password.');
					}
				}				
			}else{
				$cpass = false;
			}
			if($fval === true)	{
				if($cpass == true)	{
					$pass = encryption(1, $password);
				}else{
					$pass = $user['password'];
				}
				mysqli_query($koneksi, "update user set akses='{$akses}', nama='{$nama}', alamat='{$alamat}', telepon='{$telepon}', email='{$email}', password='{$pass}', status='{$status}' where id={$id}");
				// $user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where email='{$email}'"));
				set_message('msg', 'success', "Anda baru saja mengedit user: <strong>{$nama} (e-mail: {$email})</strong>.");
				redirect('./user.php');
			}
		}
	}
}elseif($mode == 'hapus')	{
	$id = input_get('id');
	if(in_array($id, $config['hidden_users']) or $id == session_get('uid'))	{
		set_message('msg', 'danger', 'Anda tidak memiliki hak menghapus user tersebut.');
		redirect('./user.php');
	}else{
		$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where id={$id}"));
		mysqli_query($koneksi, "delete from user where id={$id}");
		set_message('msg', 'danger', "Anda baru saja menghapus user: <strong>{$user['nama']} (email: {$user['email']})</strong>.");
		redirect('./user.php');
	}
}
require_once './header.php';
require_once './navigasi.php';
if($mode == 'tambah')	{
	?>
	<div class="body-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-group fa-fw"></i> Tambah user</h1>
					<div class="row">
						<div class="col-sm-2">
							<div class="list-group">
								<a href="./user.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
							</div>
						</div>
						<div class="col-sm-10">
							<form method="post" action="" class="form-horizontal">
								<div class="form-group">
									<label for="akses" class="control-label col-sm-3">Akses</label>
									<div class="col-sm-3">
										<select name="akses" id="akses" class="form-control">
											<option value="administrator" <?php echo form_set_dropdown('akses', 'administrator'); ?>>Administrator</option>
											<option value="operator" <?php echo form_set_dropdown('akses', 'operator'); ?>>Operator</option>
											<option value="member" <?php echo form_set_dropdown('akses', 'member', true); ?>>Member</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="nama" class="control-label col-sm-3">Nama</label>
									<div class="col-sm-5">
										<input type="text" name="nama" id="nama" class="form-control" value="<?php echo form_set_value('nama'); ?>">
										<?php echo show_error(1, $form_error, 'nama'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="alamat" class="control-label col-sm-3">Alamat</label>
									<div class="col-sm-6">
										<input type="text" name="alamat" id="alamat" class="form-control" value="<?php echo form_set_value('alamat'); ?>">
										<?php echo show_error(1, $form_error, 'alamat'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="telepon" class="control-label col-sm-3">Telepon</label>
									<div class="col-sm-3">
										<input type="text" name="telepon" id="telepon" class="form-control" value="<?php echo form_set_value('telepon'); ?>">
										<?php echo show_error(1, $form_error, 'telepon'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-3">E-mail</label>
									<div class="col-sm-6">
										<input type="text" name="email" id="email" class="form-control" value="<?php echo form_set_value('email'); ?>">
										<?php echo show_error(1, $form_error, 'email'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="control-label col-sm-3">Password</label>
									<div class="col-sm-4">
										<input type="password" name="password" id="password" class="form-control">
										<?php echo show_error(1, $form_error, 'password'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="konfirmasi" class="control-label col-sm-3">Konfirmasi</label>
									<div class="col-sm-4">
										<input type="password" name="konfirmasi" id="konfirmasi" class="form-control">
										<?php echo show_error(1, $form_error, 'konfirmasi'); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" name="ok" value="simpan" class="btn btn-primary" id="ok"><i class="fa fa-save fa-fw"></i> Simpan</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}elseif($mode == 'edit')	{
	?>
	<div class="body-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-group fa-fw"></i> Tambah user</h1>
					<div class="row">
						<div class="col-sm-2">
							<div class="list-group">
								<a href="./user.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
							</div>
						</div>
						<div class="col-sm-10">
							<form method="post" action="" class="form-horizontal">
								<div class="form-group">
									<label for="akses" class="control-label col-sm-3">Akses</label>
									<div class="col-sm-3">
										<select name="akses" id="akses" class="form-control">
											<option value="administrator" <?php echo form_set_dropdown('akses', 'administrator', val_compare('administrator', $user['akses'])); ?>>Administrator</option>
											<option value="operator" <?php echo form_set_dropdown('akses', 'operator', val_compare('operator', $user['akses'])); ?>>Operator</option>
											<option value="member" <?php echo form_set_dropdown('akses', 'member', val_compare('member', $user['akses'])); ?>>Member</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="nama" class="control-label col-sm-3">Nama</label>
									<div class="col-sm-5">
										<input type="text" name="nama" id="nama" class="form-control" value="<?php echo form_set_value('nama',$user['nama']); ?>">
										<?php echo show_error(1, $form_error, 'nama'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="alamat" class="control-label col-sm-3">Alamat</label>
									<div class="col-sm-6">
										<input type="text" name="alamat" id="alamat" class="form-control" value="<?php echo form_set_value('alamat',$user['alamat']); ?>">
										<?php echo show_error(1, $form_error, 'alamat'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="telepon" class="control-label col-sm-3">Telepon</label>
									<div class="col-sm-3">
										<input type="text" name="telepon" id="telepon" class="form-control" value="<?php echo form_set_value('telepon', $user['telepon']); ?>">
										<?php echo show_error(1, $form_error, 'telepon'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-3">E-mail</label>
									<div class="col-sm-6">
										<input type="text" name="email" id="email" class="form-control" value="<?php echo form_set_value('email', $user['email']); ?>">
										<?php echo show_error(1, $form_error, 'email'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="control-label col-sm-3">Password</label>
									<div class="col-sm-4">
										<input type="password" name="password" id="password" class="form-control">
										<?php echo show_error(1, $form_error, 'password'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="konfirmasi" class="control-label col-sm-3">Konfirmasi</label>
									<div class="col-sm-4">
										<input type="password" name="konfirmasi" id="konfirmasi" class="form-control">
										<?php echo show_error(1, $form_error, 'konfirmasi'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="status" class="control-label col-sm-3">Status</label>
									<div class="col-sm-3">
										<select name="status" id="status" class="form-control">
											<option value="aktif" <?php echo form_set_dropdown('status', 'aktif', val_compare('aktif', $user['status'])); ?>>Aktif</option>
											<option value="nonaktif" <?php echo form_set_dropdown('status', 'nonaktif', val_compare('nonaktif', $user['status'])); ?>>Nonaktif</option>											
										</select>										
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" name="ok" value="simpan" class="btn btn-primary" id="ok"><i class="fa fa-save fa-fw"></i> Simpan</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}else{
	$hu = array_processor(1, $config['hidden_users']);
?>
	<div class="body-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-group fa-fw"></i> User</h1>
					<?php echo get_message('msg'); ?>
					<div class="row">
						<div class="col-sm-2">
							<div class="list-group">
								<a href="./user.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
								<a href="./user.php?mode=tambah" class="list-group-item"><i class="fa fa-plus-circle fa-fw"></i> Tambah</a>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No.</th>
											<th>Nama</th>
											<th>Telepon</th>
											<th>E-mail</th>
											<th>Akses</th>
											<th>Status</th>
											<th class="text-center">Opsi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$nomor = 1;
										$user_query = mysqli_query($koneksi, "select * from user where id not in {$hu} order by akses asc, nama asc");
										while($user = mysqli_fetch_assoc($user_query))	{
											echo '<tr>';
											echo '<td>'.$nomor.'</td>';
											echo '<td>'.$user['nama'].'</td>';
											echo '<td>'.$user['telepon'].'</td>';
											echo '<td>'.$user['email'].'</td>';
											echo '<td>'.$user['akses'].'</td>';
											echo '<td>'.user_status(2, $user['status']).'</td>';
											echo '<td class="text-center"><a href="./user.php?mode=edit&id='.$user['id'].'"><i class="fa fa-edit fa-fw"></i></a> <a href="./user.php?mode=hapus&id='.$user['id'].'" class="hapus"><i class="fa fa-trash fa-fw"></i></a></td>';
											echo '</tr>';
											$nomor++;
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>
<script>
	$('.hapus').click(function()	{
		return confirm('Apakah Anda yakin ingin menghapus data user tersebut?');
	});
</script>
<?php
require_once './footer.php';