<?php
error_reporting(0);
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
auth(3, $config['admin_akses']);
require_once './header.php';
require_once './navigasi.php';
// if (isset($_POST['report'])){
		// $laporan=($_POST['laporan']);
		// $laporan1=substr($laporan,0,10);
		// $laporan2=substr($laporan,12,22);
		// echo $laporan1."<br />";
		// echo $laporan2;
// }
?>
<?php

$DS = DIRECTORY_SEPARATOR;
file_exists(__DIR__ . $DS . 'core' . $DS . 'Handler.php') ? require_once __DIR__ . $DS . 'core' . $DS . 'Handler.php' : die('Handler.php not found');
file_exists(__DIR__ . $DS . 'core' . $DS . 'Config.php') ? require_once __DIR__ . $DS . 'core' . $DS . 'Config.php' : die('Config.php not found');

use AjaxLiveSearch\core\Config;
use AjaxLiveSearch\core\Handler;

if (session_id() == '') {
    session_start();
}

    $handler = new Handler();
    $handler->getJavascriptAntiBot();
?>


<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <style media="screen">
    
      #message{
        display: none;
      }
    </style>
	<script src="https://code.jquery.com/jquery-1.12.4.js">

</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js">

</script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js">

</script>
<script>
	$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy','print'
        ]
    } );
} );
</script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" />

<link rel="stylesheet" href="css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="css/buttons.dataTables.min.css" />

<!-- Include Required Prerequisites -->

<script type="text/javascript" src="js/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
</head>
<body>

<div class="col-lg-12">
		<div class="form-group">
				
					<form action="mpdf/report.php" method="post">
						<table id="table">
							<tr>
								
								<td>
									Pilih Tanggal
								</td>
								<td>
									:
								</td>
								<td>
									&nbsp;&nbsp;<input type="text" id="daterange" name="laporan" /> 
								</td>
								<td>
									&nbsp;&nbsp;<input type="submit" name="report" value="DOWNLOAD" class="btn btn-info" />
								</td>
							</tr>
						</table>
					</form>
					 
			</div>
			<!-- Search Form Demo -->

		<table id="example" class="display nowrap" width="100%" cellspacing="0">
				<thead>
					<tr>
						<!--<th>No</th>!-->
						<th>ID User</th>
						<th>Kode Pesanan</th>
						<th>Id Transaksi</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>Email</th>
						<th>Bank Asal</th>
						<th>Bank Tujuan</th>
						<th>Lampiran</th>
						<th>Rekening Asal</th>
						<th>Gambar</th>
						<th>Jumlah</th>
						<th>Waktu</th>
					</tr>
				</thead>
				<tbody>
			   <?php 
					 $query=$koneksi->query("SELECT *  from konfirmasi ORDER BY id  DESC");
				  
					//echo"good";
				  
				  while($data=mysqli_fetch_array($query)){
				  $no = 1;
				  
				?>
        
				<tr>
					<!--<td><?php echo $no ++;?></td>!-->
					<td><?php echo $data['id_user'];?></td>
					<td><?php echo $data['kode_pesanan'];?></td>
					<td><?php echo $data['id_transaksi'];?></td>
					<td><?php echo $data['nama'];?></td>
					<td><?php echo $data['alamat'];?></td>
					<td><a href="mailto:<?php echo $data['email'];?></a>"><?php echo $data['email'];?></a></td>
					<td><?php echo $data['bank_asal'];?></td>
					<td><?php echo $data['bank_tujuan'];?></td>
					<td><?php echo $data['lampiran'];?></td>
					<td><?php echo $data['rekening_asal'];?></td>
					<td>&nbsp;&nbsp;<a href="../gambar/<?php echo $data ['gambar']; ?>"><img src="../gambar/<?php echo $data ['gambar']; ?>" height="50" width="50" /></a></td>
					<td><?php echo $data['jumlah'];?></td>
					<td><?php echo $data['waktu'];?></td>
				</tr>
            
				<?php
					}
				?>
			</tbody>
		</table>
  </div>
  
</body>
<script type="text/javascript">
		$(function() {
			$('#daterange').daterangepicker({
				locale: {
				  format: 'YYYY-MM-DD'
			},
   
			}, 
					function(start, end, label) {
						alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
				});
			});
</script>

<!-- Live Search Script -->

</html>