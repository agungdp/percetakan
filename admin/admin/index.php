<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
if(auth(2, $config['admin_akses']) === false)	{
	redirect('./masuk.php');
}
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-dashboard fa-fw"></i> Dashboard</h1>
			</div>
		
					<div class="col-sm-2">
						<div class="list-group">
							<a href="./ubah_password.php" class="list-group-item"><i class="fa fa-dashboard fa-fw"></i> Ubah password</a>
							<a href="./keluar.php" class="list-group-item"><i class="fa fa-sign-out fa-fw"></i> Keluar</a>
						</div>
					</div>
				</div>
			
		
	</div>
	
</div>
<?php
require_once './footer.php';