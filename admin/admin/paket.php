<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
auth(3, $config['admin_akses']);
$form_error = null;
$mode = input_get('mode');
if($mode == 'tambah')	{
	if(input_post('ok'))	{
		$nama = input_post('nama');
		$harga_dasar = input_post('harga_dasar');
		$harga_halaman = input_post('harga_halaman');
		$min_halaman = input_post('min_halaman');
		$max_halaman = input_post('max_halaman');
		$keterangan = input_post('keterangan');
		$gbr = input_post('gambar');
		$fval = true;
		$uploaddir = "../gambar//";
		$fileku = ($_FILES ['gbr']['name']);
		$tmpName  = $_FILES['gbr']['tmp_name']; 
		$uploadfile = $uploaddir.$fileku;
		echo $fileku;
		if(strlen($nama) < 1)	{
			$fval = false;
			$form_error['nama'] = error_generator(1, 'Nama harus diisi.');
		}else{
			if(mysqli_num_rows(mysqli_query($koneksi, "select id from paket where nama='{$nama}'")) > 0)	{
				$fval = false;
				$form_error['nama'] = error_generator(1, 'Nama tersebut telah digunakan.');
			}
		}
		if(strlen($harga_dasar) < 1)	{
			$fval = false;
			$form_error['harga_dasar'] = error_generator(1, 'Harga dasar harus diisi.');
		}else{
			if(filter_var($harga_dasar, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['harga_dasar'] = error_generator(1, 'Harga dasar hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(strlen($harga_halaman) < 1)	{
			$fval = false;
			$form_error['harga_halaman'] = error_generator(1, 'Harga per halaman isi harus diisi.');
		}else{
			if(filter_var($harga_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['harga_halaman'] = error_generator(1, 'Harga per halaman isi hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(strlen($min_halaman) < 1)	{
			$fval = false;
			$form_error['min_halaman'] = error_generator(1, 'Jumlah minimal halaman harus diisi.');
		}else{
			if(filter_var($min_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['min_halaman'] = error_generator(1, 'Jumlah minimal halaman hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(strlen($max_halaman) < 1)	{
			$fval = false;
			$form_error['max_halaman'] = error_generator(1, 'Jumlah maksimal halaman harus diisi.');
		}else{
			if(filter_var($max_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['max_halaman'] = error_generator(1, 'Jumlah maksimal halaman hanya boleh diisi dengan bilangan bulat.');
			}
		}
		
		if (move_uploaded_file($tmpName,$uploadfile)) 
  {
			
			mysqli_query($koneksi, "insert into paket(gambar, nama, harga_dasar, harga_halaman, min_halaman, max_halaman, keterangan) values('$fileku','$nama',$harga_dasar,$harga_halaman,$min_halaman,$max_halaman,'$keterangan')");
			$paket = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from paket where nama='$nama'"));
			set_message('msg', 'success', "Anda baru saja menambahkan paket baru: <strong>{$paket['nama']}</strong>.");
			redirect('./paket.php');
		}
	}
}elseif($mode == 'edit')	{
	$id = input_get('id');
	$paket = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from paket where id=$id"));
	if(input_post('ok'))	{
		$nama = input_post('nama');
		$harga_dasar = input_post('harga_dasar');
		$harga_halaman = input_post('harga_halaman');
		$min_halaman = input_post('min_halaman');
		$max_halaman = input_post('max_halaman');
		$keterangan = input_post('keterangan');
		$fval = true;
		$uploaddir = "../gambar//";
		$fileku = ($_FILES ['gbr']['name']);
		$tmpName  = $_FILES['gbr']['tmp_name']; 
		$uploadfile = $uploaddir.$fileku;
		echo $fileku;
		if(strlen($nama) < 1)	{
			$fval = false;
			$form_error['nama'] = error_generator(1, 'Nama harus diisi.');
		}else{
			if(mysqli_num_rows(mysqli_query($koneksi, "select id from paket where nama='{$nama}' and id!=$id")) > 0)	{
				$fval = false;
				$form_error['nama'] = error_generator(1, 'Nama tersebut telah digunakan.');
			}
		}
		if(strlen($harga_dasar) < 1)	{
			$fval = false;
			$form_error['harga_dasar'] = error_generator(1, 'Harga dasar harus diisi.');
		}else{
			if(filter_var($harga_dasar, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['harga_dasar'] = error_generator(1, 'Harga dasar hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(strlen($harga_halaman) < 1)	{
			$fval = false;
			$form_error['harga_halaman'] = error_generator(1, 'Harga per halaman isi harus diisi.');
		}else{
			if(filter_var($harga_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['harga_halaman'] = error_generator(1, 'Harga per halaman isi hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(strlen($min_halaman) < 1)	{
			$fval = false;
			$form_error['min_halaman'] = error_generator(1, 'Jumlah minimal halaman harus diisi.');
		}else{
			if(filter_var($min_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['min_halaman'] = error_generator(1, 'Jumlah minimal halaman hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(strlen($max_halaman) < 1)	{
			$fval = false;
			$form_error['max_halaman'] = error_generator(1, 'Jumlah maksimal halaman harus diisi.');
		}else{
			if(filter_var($max_halaman, FILTER_VALIDATE_INT) === false)	{
				$fval = false;
				$form_error['max_halaman'] = error_generator(1, 'Jumlah maksimal halaman hanya boleh diisi dengan bilangan bulat.');
			}
		}
		if(isset($fileku))
			if (move_uploaded_file($tmpName,$uploadfile)) 
		{
			// mysqli_query($koneksi, "insert into paket(gambar, nama, harga_dasar, harga_halaman, min_halaman, max_halaman, keterangan) values('$gbr','$nama',$harga_dasar,$harga_halaman,$min_halaman,$max_halaman,'$keterangan')");
			$sukses = mysqli_query($koneksi, "update paket set gambar='{$fileku}', nama='{$nama}', harga_dasar={$harga_dasar}, harga_halaman={$harga_halaman},min_halaman={$min_halaman},max_halaman={$max_halaman}, keterangan='{$keterangan}' where id={$id}");
			//$paket = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from paket where nama='$nama'"));
			if($sukses){
				set_message('msg', 'warning', "Anda baru saja mengedit paket: <strong>{$nama}</strong>.");
			redirect('./paket.php');
			}else{
				echo "gagal";
			}
			
		}else{
			// mysqli_query($koneksi, "insert into paket(gambar, nama, harga_dasar, harga_halaman, min_halaman, max_halaman, keterangan) values('$gbr','$nama',$harga_dasar,$harga_halaman,$min_halaman,$max_halaman,'$keterangan')");
			$sukses = mysqli_query($koneksi, "update paket set  nama='{$nama}', harga_dasar={$harga_dasar}, harga_halaman={$harga_halaman},min_halaman={$min_halaman},max_halaman={$max_halaman}, keterangan='{$keterangan}' where id={$id}");
			//$paket = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from paket where nama='$nama'"));
			if($sukses){
				set_message('msg', 'warning', "Anda baru saja mengedit paket: <strong>{$nama}</strong>.");
			redirect('./paket.php');
			}else{
				echo "gagal";
			}
		}
	}
	}
elseif ($mode == 'hapus') {
	$id = input_get('id');
	$paket = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from paket where id={$id}"));
	mysqli_query($koneksi, "delete from paket where id={$id}");
	unlink('../assets/upload/'.$paket['gambar']);
	set_message('msg', 'danger', "Anda baru saja menghapus paket: <strong>{$paket['nama']}</strong>.");
	redirect('./paket.php');
}
require_once './header.php';
require_once './navigasi.php';
if($mode == 'tambah')	{
	?>
	<div class="body-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-file-archive-o fa-fw"></i> Tambah paket</h1>
					<div class="row">
						<div class="col-sm-2">
							<div class="list-group">
								<a href="./paket.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
							</div>
						</div>
						<div class="col-sm-10">
							<form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
								<div class="form-group">
									<label for="gambar" class="control-label col-sm-3">Thumbnail</label>
									
									<div class="col-sm-9">
										<input type="file" name="gbr" id="gbr">
										<?php echo show_error(1, $form_error, 'gambar'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="nama" class="control-label col-sm-3">Nama</label>
									<div class="col-sm-5">
										<input type="text" name="nama" id="nama" class="form-control" value="<?php echo form_set_value('nama'); ?>">
										<?php echo show_error(1, $form_error, 'nama'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="harga_dasar" class="control-label col-sm-3">Harga dasar</label>
									<div class="col-sm-5">
										<input type="text" name="harga_dasar" id="harga_dasar" maxlength="10" class="form-control" value="<?php echo form_set_value('harga_dasar'); ?>">
										<?php echo show_error(1, $form_error, 'harga_dasar'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="harga_halaman" class="control-label col-sm-3">Harga per halaman isi</label>
									<div class="col-sm-5">
										<input type="text" name="harga_halaman" id="harga_halaman" maxlength="10" class="form-control" value="<?php echo form_set_value('harga_halaman'); ?>">
										<?php echo show_error(1, $form_error, 'harga_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="min_halaman" class="control-label col-sm-3">Jumlah minimal halaman</label>
									<div class="col-sm-5">
										<input type="text" name="min_halaman" id="min_halaman" maxlength="10" class="form-control" value="<?php echo form_set_value('min_halaman'); ?>">
										<?php echo show_error(1, $form_error, 'min_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="max_halaman" class="control-label col-sm-3">Jumlah maksimal halaman</label>
									<div class="col-sm-5">
										<input type="text" name="max_halaman" id="max_halaman" maxlength="10" class="form-control" value="<?php echo form_set_value('max_halaman'); ?>">
										<?php echo show_error(1, $form_error, 'max_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="keterangan" class="control-label col-sm-3">Keterangan</label>
									<div class="col-sm-9">
										<textarea name="keterangan" id="keterangan" class="form-control wordpad" rows="5"><?php echo form_set_value('keterangan'); ?></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" name="ok" id="ok" value="simpan" class="btn btn-primary">
											<i class="fa fa-save fa-fw"></i> Simpan
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}elseif($mode == 'edit')	{	
	?>
	<div class="body-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-file-archive-o fa-fw"></i> Tambah paket</h1>
					<div class="row">
						<div class="col-sm-2">
							<div class="list-group">
								<a href="./paket.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
							</div>
						</div>
						<div class="col-sm-10">
							<form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
								<?php
								if(strlen($paket['gambar']) > 0)	{
									?>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-3">
											<a href="../gambar/<?php echo $paket ['gambar']; ?>"><img src="../gambar/<?php echo $paket ['gambar']; ?>" height="100" width="50" class="img-responsive" /></a>
										</div>
									</div>
									<?php
								}
								?>
								<div class="form-group">
									<label for="gambar" class="control-label col-sm-3">Thumbnail</label>
									<div class="col-sm-9">
										<input type="file" name="gbr" id="gbr">
										<?php echo show_error(1, $form_error, 'gambar'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="nama" class="control-label col-sm-3">Nama</label>
									<div class="col-sm-5">
										<input type="text" name="nama" id="nama" class="form-control" value="<?php echo form_set_value('nama', $paket['nama']); ?>">
										<?php echo show_error(1, $form_error, 'nama'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="harga_dasar" class="control-label col-sm-3">Harga dasar</label>
									<div class="col-sm-5">
										<input type="text" name="harga_dasar" id="harga_dasar" maxlength="10" class="form-control" value="<?php echo form_set_value('harga_dasar', $paket['harga_dasar']); ?>">
										<?php echo show_error(1, $form_error, 'harga_dasar'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="harga_halaman" class="control-label col-sm-3">Harga per halaman isi</label>
									<div class="col-sm-5">
										<input type="text" name="harga_halaman" id="harga_halaman" maxlength="10" class="form-control" value="<?php echo form_set_value('harga_halaman', $paket['harga_halaman']); ?>">
										<?php echo show_error(1, $form_error, 'harga_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="min_halaman" class="control-label col-sm-3">Jumlah minimal halaman</label>
									<div class="col-sm-5">
										<input type="text" name="min_halaman" id="min_halaman" maxlength="10" class="form-control" value="<?php echo form_set_value('min_halaman', $paket['min_halaman']); ?>">
										<?php echo show_error(1, $form_error, 'min_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="max_halaman" class="control-label col-sm-3">Jumlah maksimal halaman</label>
									<div class="col-sm-5">
										<input type="text" name="max_halaman" id="max_halaman" maxlength="10" class="form-control" value="<?php echo form_set_value('max_halaman', $paket['max_halaman']); ?>">
										<?php echo show_error(1, $form_error, 'max_halaman'); ?>
									</div>
								</div>
								<div class="form-group">
									<label for="keterangan" class="control-label col-sm-3">Keterangan</label>
									<div class="col-sm-9">
										<textarea name="keterangan" id="keterangan" class="form-control wordpad" rows="5"><?php echo form_set_value('keterangan', $paket['keterangan']); ?></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" name="ok" id="ok" value="simpan" class="btn btn-primary">
											<i class="fa fa-save fa-fw"></i> Simpan
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}else{
	?>
	<div class="body-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="title1"><i class="fa fa-file-archive-o fa-fw"></i> Paket cetak</h1>
					<?php echo get_message('msg'); ?>
					<div class="row">
						<div class="col-sm-2">
							<div class="list-group">
								<a href="./paket.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
								<a href="./paket.php?mode=tambah" class="list-group-item"><i class="fa fa-plus-circle fa-fw"></i> Tambah</a>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No.</th>
											<th>Nama</th>
											<th class="text-right">Harga dasar</th>
											<th class="text-right">Harga halaman</th>
											<th class="text-right">Min. Halaman</th>
											<th class="text-right">Max. Halaman</th>
											<th class="text-center">Opsi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$nomor = 1;
										$paket_query = mysqli_query($koneksi, "select * from paket order by nama asc");
										while($paket = mysqli_fetch_assoc($paket_query))	{
											echo '<tr>';
											echo '<td>'.$nomor.'</td>';
											echo '<td>'.$paket['nama'].'</td>';
											echo '<td class="text-right">'.rupiah($paket['harga_dasar']).'</td>';
											echo '<td class="text-right">'.rupiah($paket['harga_halaman']).'</td>';
											echo '<td class="text-right">'.$paket['min_halaman'].'</td>';
											echo '<td class="text-right">'.$paket['max_halaman'].'</td>';
											echo '<td class="text-center"><a href="./paket.php?mode=edit&id='.$paket['id'].'"><i class="fa fa-edit fa-fw"></i></a> <a href="./paket.php?mode=hapus&id='.$paket['id'].'" class="hapus"><i class="fa fa-trash fa-fw"></i></a></td>';
											echo '</tr>';
											$nomor++;
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>
<script>
	$('.hapus').click(function()	{
		return confirm('Apakah Anda yakin ingin menghapus paket tersebut?');
	});
</script>
<?php
require_once './footer.php';