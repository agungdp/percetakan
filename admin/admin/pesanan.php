<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
auth(3, $config['admin_akses']);
require_once './header.php';
require_once './navigasi.php';
?>
<html>
	<head>
		<title>
			Pesanan
		</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <style media="screen">
    
      #message{
        display: none;
      }
    </style>
	<script src="https://code.jquery.com/jquery-1.12.4.js">

	</script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">

	</script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js">

	</script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js">

	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">

	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js">

	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js">

	</script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js">

	</script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js">

	</script>
	<script>
		$(document).ready(function() {
		$('#example').DataTable( {
			dom: 'Bfrtip',
			buttons: [
				'copy', 'print'
			]
		} );
	} );
	</script>
	<link rel="stylesheet" href="css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="css/buttons.dataTables.min.css" />

	<!-- Include Required Prerequisites -->

	<script type="text/javascript" src="js/moment.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="js/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
		
	</head>
	<body>
			<div class="body-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<h1 class="title1"><i class="fa fa-shopping-cart fa-fw"></i> Pesanan</h1>
								<?php
									if(isset ($_SESSION['uemail'])){
									$a=$_SESSION['uemail'];
									$query = $koneksi ->query("SELECT id,nama from user where email LIKE'".$a."%'");
									 if($data=mysqli_fetch_array($query)){
								?>
								<form action="mpdf/report.php" method="post">
									<table id="table">
										<tr>
											<td>
												<input type="hidden"  name="id" value="<?php echo strtoupper($data['id'])?>"/> 
											</td>
											<td>
												Pilih Tanggal
											</td>
											<td>
												:
											</td>
											<td>
												&nbsp;&nbsp;<input type="text" id="daterange" name="laporan" /> 
											</td>
											<td>
												&nbsp;&nbsp;<input type="submit" name="report1" value="DOWNLOAD" class="btn btn-info" />
											</td>
										</tr>
									</table>
								</form>
								<?php
									
									}else{
										echo "silahkan login";
									}
									}
						
								?>
							<div class="row">
								<div class="col-sm-2">
									<div class="list-group">
										<a href="./pesanan.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
									</div>
								</div>
								<div class="col-sm-10">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Kode pesanan</th>
												<th>User</th>
												<th>Paket</th>
												<th class="text-right">Jumlah isi</th>
												<th class="text-right">Total harga</th>
												<th class="text-center">Waktu pemesanan</th>
												<th class="text"-center">Waktu Selesai</th>
												<th>Status</th>
												<th class="text-center">Opsi</th>
											</tr>
										</thead>				
										<tbody>
											<?php
											$pesanan_query = mysqli_query($koneksi, "select up.*,u.nama as nama_user,p.nama as paket from user_paket up join paket p on p.id=up.id_paket join user u on u.id=up.id_user order by up.status asc, up.waktu_pemesanan asc");
											while($pesanan = mysqli_fetch_assoc($pesanan_query))	{
												echo '<tr>';
												echo '<td>'.$pesanan['kode_pesanan'].'</td>';
												echo '<td>'.$pesanan['nama_user'].'</td>';
												echo '<td>'.$pesanan['paket'].'</td>';
												echo '<td class="text-right">'.$pesanan['jumlah_halaman'].'</td>';
												echo '<td class="text-right">'.rupiah($pesanan['total_harga']).'</td>';
												echo '<td class="text-center">'.$pesanan['waktu_pemesanan'].'</td>';
												echo '<td class="text-center">'.$pesanan['waktu_selesai'].'</td>>';
												echo '<td>'.pesanan_status(2, $pesanan['status']).'</td>';
												echo '<td class="text-center"><a href="edit_transaksi.php?mode=edit&id='.$pesanan['id_user_paket'].'"><i class="fa fa-edit fa-fw"></i></a></td>';
												
												echo '</tr>';
											}
											?>
						
										</tbody>				
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</body>
		<script type="text/javascript">
		$(function() {
			$('#daterange').daterangepicker({
				locale: {
				  format: 'YYYY-MM-DD'
			},
   
			}, 
					function(start, end, label) {
						alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
				});
			});
		</script>		
		<?php	
		require_once './footer.php';
		?>
</html>