<?php
include ("../koneksi/connect.php");
if(isset($_POST['download_report_shift'])){
			if($_POST['typereportshiftyearly']=="PDF"){
					if(($_POST['shift']=="FLOW")){
						$sql=pg_query($conn,"SELECT * FROM result WHERE flow !=0  ");
							if(pg_num_rows($sql)){
						require('fpdf17/fpdf.php');
						$tgl1=$_POST['tgl1'];
						$tgl2=$_POST['tgl2'];
						//$date=explode("-",$tgl1);
						 //mencari element array 0
						 //$tahun = $date[0];
						 $tanggal= "Date :".$tgl1; 
						$shift=$_POST['shift'];
						$date_report="REPORT YEARLY  : KATEGORI ".$shift;
						$result=pg_query($conn,"SELECT * FROM result WHERE flow !=0 AND BETWEEN date_trunc('day', start)='$tgl1' AND date_trunc('day', start)='$tgl2' ")or die("Database Error");

					//Inisiasi untuk membuat header kolom
					$column_nik = "";
					$column_nama = "";
					$column_tempat = "";
					$column_tanggal = "";
					$column_alamat = "";
					

					//For each row, add the field to the corresponding column
					while($row = pg_fetch_array($result))
					{
						$nik = $row["flow"];
						$nama = $row["start"];
						$tempat_lahir = $row["stop"];
						
						

						$column_nik = $column_nik.$nik."\n";
						$column_nama = $column_nama.$nama."\n";
						$column_tempat = $column_tempat.$tempat_lahir."\n";
						
						

					//Create a new PDF file
					$pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
					$pdf->AddPage();

					//Menambahkan Gambar
					//$pdf->Image('../foto/logo.png',10,10,-175);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(80);
					$pdf->Cell(30,10,'LAPORAN HASIL PRODUKSI',0,0,'C');
					$pdf->SetFont('Arial','B',15);
					$pdf->Cell(-30,25,'PT COCA COLA',0,0,'C');
					
					//detail date
					$pdf->Cell(120);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(-70,10,$date_report,0,0,'C');
					$pdf->SetFont('Arial','B',12);
					$pdf->Cell(59,25,$tanggal,0,0,'C');
					
					//detail logo
					$pdf->Cell(-120);
					$pdf->SetFont('Arial','B',10);
					$pdf->Image('../gambar/cocacola.png',10,10,-300);
					
					
					//Border
					//$pdf->Cell(160,10,$tgl,1,1,'C');
					$pdf->Ln();
					$pdf->Cell(80);
					$pdf->Ln();

					}
					//Fields Name position
				 $Y_Fields_Name_position = 35;

					//First create each Field Name
					//Gray color filling each Field Name box
					$pdf->SetFillColor(110,180,230);
					//Bold Font for Field Name
					$pdf->SetFont('Arial','B',8);
					$pdf->SetY($Y_Fields_Name_position);
					$pdf->SetX(5);
					$pdf->Cell(20,8,'FLOW',1,0,'C',1);
					$pdf->SetX(25);
					$pdf->Cell(20,8,'TIME START',1,0,'C',1);
					$pdf->SetX(45);
					$pdf->Cell(45,8,'TIME FINISH',1,0,'C',1);
					$pdf->SetX(90);
					
					
					$pdf->Ln();

					//Table position, under Fields Name
					$Y_Table_Position = 43;

					//Now show the columns
					$pdf->SetFont('Arial','B',5);

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(5);
					$pdf->MultiCell(20,6,$column_nik,1,'C');

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(25);
					$pdf->MultiCell(20,6,$column_nama,1,'C');

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(45);
					$pdf->MultiCell(45,6,$column_tempat,1,'L');

					

					 

					$pdf->Output();

					}else{
						header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
						 }
					}else if (($_POST['shift']=="BRIX")){
						$sql=pg_query($conn,"SELECT * FROM result WHERE brix !=0  ");
							if(pg_num_rows($sql)){
						require('fpdf17/fpdf.php');
						$tgl=$_POST['tgl'];
						$date=explode("-",$tgl);
						 //mencari element array 0
						 $tahun = $date[0];
						 $tanggal= "Date :".$tahun;
						$shift=$_POST['shift'];
						$date_report="REPORT YEARLY  : KATEGORI ".$shift;
						$result=pg_query($conn,"SELECT * FROM result WHERE brix !=0 AND date_trunc('year', start)='$tgl' ")or die("Database Error");

					//Inisiasi untuk membuat header kolom
					$column_nik = "";
					$column_nama = "";
					$column_tempat = "";
					$column_tanggal = "";
					$column_alamat = "";
					

					//For each row, add the field to the corresponding column
					while($row = pg_fetch_array($result))
					{
						$nik = $row["brix"];
						$nama = $row["start"];
						$tempat_lahir = $row["stop"];
						
						

						$column_nik = $column_nik.$nik."\n";
						$column_nama = $column_nama.$nama."\n";
						$column_tempat = $column_tempat.$tempat_lahir."\n";
						
						

					//Create a new PDF file
					$pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
					$pdf->AddPage();

					//Menambahkan Gambar
					//$pdf->Image('../foto/logo.png',10,10,-175);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(80);
					$pdf->Cell(30,10,'LAPORAN HASIL PRODUKSI',0,0,'C');
					$pdf->SetFont('Arial','B',15);
					$pdf->Cell(-30,25,'PT COCA COLA',0,0,'C');
					
					//detail date
					$pdf->Cell(120);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(-70,10,$date_report,0,0,'C');
					$pdf->SetFont('Arial','B',12);
					$pdf->Cell(59,25,$tanggal,0,0,'C');
					
					//detail logo
					$pdf->Cell(-120);
					$pdf->SetFont('Arial','B',10);
					$pdf->Image('../gambar/cocacola.png',10,10,-300);
					
					
					//Border
					//$pdf->Cell(160,10,$tgl,1,1,'C');
					$pdf->Ln();
					$pdf->Cell(80);
					$pdf->Ln();

					}
					//Fields Name position
				 $Y_Fields_Name_position = 35;

					//First create each Field Name
					//Gray color filling each Field Name box
					$pdf->SetFillColor(110,180,230);
					//Bold Font for Field Name
					$pdf->SetFont('Arial','B',8);
					$pdf->SetY($Y_Fields_Name_position);
					$pdf->SetX(5);
					$pdf->Cell(20,8,'BRIX',1,0,'C',1);
					$pdf->SetX(25);
					$pdf->Cell(20,8,'TIME START',1,0,'C',1);
					$pdf->SetX(45);
					$pdf->Cell(45,8,'TIME FINISH',1,0,'C',1);
					$pdf->SetX(90);
					
					
					$pdf->Ln();

					//Table position, under Fields Name
					$Y_Table_Position = 43;

					//Now show the columns
					$pdf->SetFont('Arial','B',5);

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(5);
					$pdf->MultiCell(20,6,$column_nik,1,'C');

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(25);
					$pdf->MultiCell(20,6,$column_nama,1,'C');

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(45);
					$pdf->MultiCell(45,6,$column_tempat,1,'L');
					$pdf->Output();
					}else{
						header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
						 }

					}else if (($_POST['shift']=="MASS")){
						$sql=pg_query($conn,"SELECT * FROM result WHERE mass !=0  ");
							if(pg_num_rows($sql)){
						require('fpdf17/fpdf.php');
						$tgl=$_POST['tgl'];
						$date=explode("-",$tgl);
						 //mencari element array 0
						 $tahun = $date[0];
						 $tanggal= "Date :".$tahun;
						$shift=$_POST['shift'];
						$date_report="REPORT YEARLY  : KATEGORI ".$shift;
						$result=pg_query($conn,"SELECT * FROM result WHERE mass !=0 AND date_trunc('year', start)='$tgl' ")or die("Database Error");

					//Inisiasi untuk membuat header kolom
					$column_nik = "";
					$column_nama = "";
					$column_tempat = "";
					$column_tanggal = "";
					$column_alamat = "";
					

					//For each row, add the field to the corresponding column
					while($row = pg_fetch_array($result))
					{
						$nik = $row["mass"];
						$nama = $row["start"];
						$tempat_lahir = $row["stop"];
						
						

						$column_nik = $column_nik.$nik."\n";
						$column_nama = $column_nama.$nama."\n";
						$column_tempat = $column_tempat.$tempat_lahir."\n";
						
						

					//Create a new PDF file
					$pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
					$pdf->AddPage();

					//Menambahkan Gambar
					//$pdf->Image('../foto/logo.png',10,10,-175);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(80);
					$pdf->Cell(30,10,'LAPORAN HASIL PRODUKSI',0,0,'C');
					$pdf->SetFont('Arial','B',15);
					$pdf->Cell(-30,25,'PT COCA COLA',0,0,'C');
					
					//detail date
					$pdf->Cell(120);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(-70,10,$date_report,0,0,'C');
					$pdf->SetFont('Arial','B',12);
					$pdf->Cell(59,25,$tanggal,0,0,'C');
					
					//detail logo
					$pdf->Cell(-120);
					$pdf->SetFont('Arial','B',10);
					$pdf->Image('../gambar/cocacola.png',10,10,-300);
					
					
					//Border
					//$pdf->Cell(160,10,$tgl,1,1,'C');
					$pdf->Ln();
					$pdf->Cell(80);
					$pdf->Ln();

					}
					//Fields Name position
				 $Y_Fields_Name_position = 35;

					//First create each Field Name
					//Gray color filling each Field Name box
					$pdf->SetFillColor(110,180,230);
					//Bold Font for Field Name
					$pdf->SetFont('Arial','B',8);
					$pdf->SetY($Y_Fields_Name_position);
					$pdf->SetX(5);
					$pdf->Cell(20,8,'MASS',1,0,'C',1);
					$pdf->SetX(25);
					$pdf->Cell(20,8,'TIME START',1,0,'C',1);
					$pdf->SetX(45);
					$pdf->Cell(45,8,'TIME FINISH',1,0,'C',1);
					$pdf->SetX(90);
					
					
					$pdf->Ln();

					//Table position, under Fields Name
					$Y_Table_Position = 43;

					//Now show the columns
					$pdf->SetFont('Arial','B',5);

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(5);
					$pdf->MultiCell(20,6,$column_nik,1,'C');

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(25);
					$pdf->MultiCell(20,6,$column_nama,1,'C');

					$pdf->SetY($Y_Table_Position);
					$pdf->SetX(45);
					$pdf->MultiCell(45,6,$column_tempat,1,'L');

					

					 

					$pdf->Output();
					}else{
						header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
						 }

					}else{
						header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
			}

				}else if($_POST['typereportshiftyearly']=="EXCEL"){
                   if(($_POST['shift']=="FLOW")){
/** Set default timezone (will throw a notice otherwise) */
						$datak=pg_query($conn,"SELECT * FROM result WHERE flow !=0 ");
						date_default_timezone_set('Asia/Jakarta');

						// include PHPExcel

					if(pg_num_rows($datak)){
						require('PHPExcel.php');
						$tgl=$_POST['tgl'];
						$date=explode("-",$tgl);
						 //mencari element array 0
						 $tahun = $date[0];
						 $tanggal= "Date :".$tahun;
						$shift=$_POST['shift'];

					$result=pg_query($conn,"SELECT * FROM result WHERE flow !=0 AND date_trunc('year', start)='$tgl' ")or die("Database Error");
						// create new PHPExcel object
						$objPHPExcel = new PHPExcel;

						// set default font
						$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

						// set default font size
						$objPHPExcel->getDefaultStyle()->getFont()->setSize(15);

						// create the writer
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");



						/**

						 * Define currency and number format.

						 */

						$currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

						// number format, with thousands separator and two decimal points.
						$numberFormat = '#,#0.##;[Red]-#,#0.##';



						// writer already created the first sheet for us, let's get it
						$objSheet = $objPHPExcel->getActiveSheet();

						// rename the sheet
						$objSheet->setTitle('COCA COLA');



						// let's bold and size the header font and write the header
						// as you can see, we can specify a range of cells, like here: cells from A1 to A4
						$objSheet->getStyle('A1:D1')->getFont()->setBold(true)->setSize(12);
						$objSheet->getStyle('A1:J1')->getFont()->setBold(true)->setSize(22);
						$objSheet->getStyle('A2:J2')->getFont()->setBold(true)->setSize(14);
						$objSheet->getStyle('A3:J3')->getFont()->setBold(true)->setSize(14);

						$objSheet->getStyle('A1:J4')->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
						);
						$objPHPExcel->getActiveSheet()
							->getStyle('A5:K5')
							->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN)
							->getColor()

							->setRGB('DDDDDD');


						// write header

						// Set document properties
						$objPHPExcel->getProperties()->setCreator("ADMIN COCA COLA")
													->setLastModifiedBy("ADMIN COCA COLA")
													->setTitle("Data Siswa")
													->setSubject("Siswa")
													->setDescription("Data siswa tahun ajaran 2015/2016")
													->setKeywords("sibangStudio PHPExcel php")
													->setCategory("Umum");

						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setPath('../gambar/cocacola1.png');
						$objDrawing->setCoordinates('A1');
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						// mulai dari baris ke 2
						$row = 5;

						// Tulis judul tabel

						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1')->setCellValue('A1', "LAPORAN HASIL PRODUKSI");
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:J2')->setCellValue('A2', "REPORT YEARLY : KATEGORI".$shift);
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:J3')->setCellValue('A3', "DATE :".$tanggal)


									->setCellValue('A5','No')
									->setCellValue('B5', 'Flow')
									->setCellValue('C5', 'START')
									->setCellValue('D5', 'STOP');
									
									

						$nomor 	= 1; // set nomor urut = 1;

						$row++; // pindah ke row bawahnya. (ke row 2)

						// lakukan perulangan untuk menuliskan data siswa
						while( $data = pg_fetch_array($result)){

							$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$row,  $nomor )
									->setCellValueExplicit('B'.$row, $data['flow'], PHPExcel_Cell_DataType::TYPE_STRING )
									->setCellValueExplicit('C'.$row, $data['start'], PHPExcel_Cell_DataType::TYPE_STRING)
									->setCellValue('D'.$row, $data['stop'] );
									
									


							$row++; // pindah ke row bawahnya ($row + 1)
							$nomor++;
						}
						// autosize the columns
						$objSheet->getColumnDimension('A')->setAutoSize(true);
						$objSheet->getColumnDimension('B')->setAutoSize(true);
						$objSheet->getColumnDimension('C')->setAutoSize(true);
						$objSheet->getColumnDimension('D')->setAutoSize(true);
						$objSheet->getColumnDimension('E')->setAutoSize(true);
						$objSheet->getColumnDimension('F')->setAutoSize(true);
						

						//Setting the header type
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="LELCOMANUFACTURING.xlsx"');
						header('Cache-Control: max-age=0');

						$objWriter->save('php://output');

						/* If you want to save the file on the server instead of downloading, replace the last 4 lines by
							$objWriter->save('test.xlsx');	*/
					}else{
							header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
						 }
				}else if(($_POST['shift']=="BRIX")){
					/** Set default timezone (will throw a notice otherwise) */
						$datak=pg_query($conn,"SELECT * FROM result WHERE brix !=0 ");
						date_default_timezone_set('Asia/Jakarta');

						// include PHPExcel

					if(pg_num_rows($datak)){
						require('PHPExcel.php');
						$tgl=$_POST['tgl'];
						$date=explode("-",$tgl);
						 //mencari element array 0
						 $tahun = $date[0];
						 $tanggal= "Date :".$tahun;
						$shift=$_POST['shift'];

					$result=pg_query($conn,"SELECT * FROM result WHERE brix !=0 AND date_trunc('year', start)='$tgl' ")or die("Database Error");
						// create new PHPExcel object
						$objPHPExcel = new PHPExcel;

						// set default font
						$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

						// set default font size
						$objPHPExcel->getDefaultStyle()->getFont()->setSize(15);

						// create the writer
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");



						/**

						 * Define currency and number format.

						 */

						$currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

						// number format, with thousands separator and two decimal points.
						$numberFormat = '#,#0.##;[Red]-#,#0.##';



						// writer already created the first sheet for us, let's get it
						$objSheet = $objPHPExcel->getActiveSheet();

						// rename the sheet
						$objSheet->setTitle('COCA COLA');



						// let's bold and size the header font and write the header
						// as you can see, we can specify a range of cells, like here: cells from A1 to A4
						$objSheet->getStyle('A1:D1')->getFont()->setBold(true)->setSize(12);
						$objSheet->getStyle('A1:J1')->getFont()->setBold(true)->setSize(22);
						$objSheet->getStyle('A2:J2')->getFont()->setBold(true)->setSize(14);
						$objSheet->getStyle('A3:J3')->getFont()->setBold(true)->setSize(14);

						$objSheet->getStyle('A1:J4')->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
						);
						$objPHPExcel->getActiveSheet()
							->getStyle('A5:K5')
							->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN)
							->getColor()

							->setRGB('DDDDDD');


						// write header

						// Set document properties
						$objPHPExcel->getProperties()->setCreator("ADMIN COCA COLA")
													->setLastModifiedBy("ADMIN COCA COLA")
													->setTitle("Data Siswa")
													->setSubject("Siswa")
													->setDescription("Data siswa tahun ajaran 2015/2016")
													->setKeywords("sibangStudio PHPExcel php")
													->setCategory("Umum");

						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setPath('../gambar/cocacola1.png');
						$objDrawing->setCoordinates('A1');
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						// mulai dari baris ke 2
						$row = 5;

						// Tulis judul tabel

						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1')->setCellValue('A1', "LAPORAN HASIL PRODUKSI");
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:J2')->setCellValue('A2', "REPORT YEARLY : KATEGORI".$shift);
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:J3')->setCellValue('A3', "DATE :".$tanggal)


									->setCellValue('A5','No')
									->setCellValue('B5', 'Brix')
									->setCellValue('C5', 'START')
									->setCellValue('D5', 'STOP');
									
									

						$nomor 	= 1; // set nomor urut = 1;

						$row++; // pindah ke row bawahnya. (ke row 2)

						// lakukan perulangan untuk menuliskan data siswa
						while( $data = pg_fetch_array($result)){

							$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$row,  $nomor )
									->setCellValueExplicit('B'.$row, $data['brix'], PHPExcel_Cell_DataType::TYPE_STRING )
									->setCellValueExplicit('C'.$row, $data['start'], PHPExcel_Cell_DataType::TYPE_STRING)
									->setCellValue('D'.$row, $data['stop'] );
									
									


							$row++; // pindah ke row bawahnya ($row + 1)
							$nomor++;
						}
						// autosize the columns
						$objSheet->getColumnDimension('A')->setAutoSize(true);
						$objSheet->getColumnDimension('B')->setAutoSize(true);
						$objSheet->getColumnDimension('C')->setAutoSize(true);
						$objSheet->getColumnDimension('D')->setAutoSize(true);
						$objSheet->getColumnDimension('E')->setAutoSize(true);
						$objSheet->getColumnDimension('F')->setAutoSize(true);
						

						//Setting the header type
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="LELCOMANUFACTURING.xlsx"');
						header('Cache-Control: max-age=0');

						$objWriter->save('php://output');

						/* If you want to save the file on the server instead of downloading, replace the last 4 lines by
							$objWriter->save('test.xlsx');	*/
                        }else{
							header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
						 }
				}else if(($_POST['shift']=="MASS")){
					/** Set default timezone (will throw a notice otherwise) */
						$datak=pg_query($conn,"SELECT * FROM result WHERE mass !=0 ");
						date_default_timezone_set('Asia/Jakarta');

						// include PHPExcel

					if(pg_num_rows($datak)){
						require('PHPExcel.php');
						$tgl=$_POST['tgl'];
						$date=explode("-",$tgl);
						 //mencari element array 0
						 $tahun = $date[0];
						 $tanggal= "Date :".$tahun;
						$shift=$_POST['shift'];

					$result=pg_query($conn,"SELECT * FROM result WHERE mass !=0 AND date_trunc('year', start)='$tgl' ")or die("Database Error");
						// create new PHPExcel object
						$objPHPExcel = new PHPExcel;

						// set default font
						$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

						// set default font size
						$objPHPExcel->getDefaultStyle()->getFont()->setSize(15);

						// create the writer
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");



						/**

						 * Define currency and number format.

						 */

						$currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

						// number format, with thousands separator and two decimal points.
						$numberFormat = '#,#0.##;[Red]-#,#0.##';



						// writer already created the first sheet for us, let's get it
						$objSheet = $objPHPExcel->getActiveSheet();

						// rename the sheet
						$objSheet->setTitle('COCA COLA');



						// let's bold and size the header font and write the header
						// as you can see, we can specify a range of cells, like here: cells from A1 to A4
						$objSheet->getStyle('A1:D1')->getFont()->setBold(true)->setSize(12);
						$objSheet->getStyle('A1:J1')->getFont()->setBold(true)->setSize(22);
						$objSheet->getStyle('A2:J2')->getFont()->setBold(true)->setSize(14);
						$objSheet->getStyle('A3:J3')->getFont()->setBold(true)->setSize(14);

						$objSheet->getStyle('A1:J4')->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
						);
						$objPHPExcel->getActiveSheet()
							->getStyle('A5:K5')
							->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN)
							->getColor()

							->setRGB('DDDDDD');


						// write header

						// Set document properties
						$objPHPExcel->getProperties()->setCreator("ADMIN COCA COLA")
													->setLastModifiedBy("ADMIN COCA COLA")
													->setTitle("Data Siswa")
													->setSubject("Siswa")
													->setDescription("Data siswa tahun ajaran 2015/2016")
													->setKeywords("sibangStudio PHPExcel php")
													->setCategory("Umum");

						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setPath('../gambar/cocacola1.png');
						$objDrawing->setCoordinates('A1');
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						// mulai dari baris ke 2
						$row = 5;

						// Tulis judul tabel

						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1')->setCellValue('A1', "LAPORAN HASIL PRODUKSI");
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:J2')->setCellValue('A2', "REPORT YEARLY : KATEGORI".$shift);
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:J3')->setCellValue('A3', "DATE :".$tanggal)


									->setCellValue('A5','No')
									->setCellValue('B5', 'Mass')
									->setCellValue('C5', 'START')
									->setCellValue('D5', 'STOP');
									
									

						$nomor 	= 1; // set nomor urut = 1;

						$row++; // pindah ke row bawahnya. (ke row 2)

						// lakukan perulangan untuk menuliskan data siswa
						while( $data = pg_fetch_array($result)){

							$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$row,  $nomor )
									->setCellValueExplicit('B'.$row, $data['mass'], PHPExcel_Cell_DataType::TYPE_STRING )
									->setCellValueExplicit('C'.$row, $data['start'], PHPExcel_Cell_DataType::TYPE_STRING)
									->setCellValue('D'.$row, $data['stop'] );
									
									


							$row++; // pindah ke row bawahnya ($row + 1)
							$nomor++;
						}
						// autosize the columns
						$objSheet->getColumnDimension('A')->setAutoSize(true);
						$objSheet->getColumnDimension('B')->setAutoSize(true);
						$objSheet->getColumnDimension('C')->setAutoSize(true);
						$objSheet->getColumnDimension('D')->setAutoSize(true);
						$objSheet->getColumnDimension('E')->setAutoSize(true);
						$objSheet->getColumnDimension('F')->setAutoSize(true);
						

						//Setting the header type
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="LELCOMANUFACTURING.xlsx"');
						header('Cache-Control: max-age=0');

						$objWriter->save('php://output');

						/* If you want to save the file on the server instead of downloading, replace the last 4 lines by
							$objWriter->save('test.xlsx');	*/
						}else{
							header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
						 }

				}else{
					header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
				}
}else{
					header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
				}
		}else {
				header("Location:http://localhost/project/jpa/cocacolasuc/report/index.php");
				

}
?>
