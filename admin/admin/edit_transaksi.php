<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
auth(3, $config['admin_akses']);
require_once './header.php';
require_once './navigasi.php';
auth(3, 'administrator');
$form_error = null;
$mode = input_get('mode');
if($mode == 'edit')	{
	$id = input_get('id');
	if(!is_numeric($id))
	{
		header('Location: '.$config['base_url'].'admin/pesanan.php');
	}
	else
	{
		$pesanan = mysqli_fetch_assoc(mysqli_query($koneksi, "select up.*,u.nama as nama_user,p.nama as paket from user_paket up join paket p on p.id=up.id_paket join user u on u.id=up.id_user where up.id_user_paket={$id} order by up.status asc, up.waktu_pemesanan asc"));
		if(!$pesanan)
		{
			header('Location: '.$config['base_url'].'admin/pesanan.php');
		}

	}
	
	if(input_post('ok'))
	{
		$waktu_selesai = empty(input_post('waktu_selesai')) ? '' : input_post('waktu_selesai');
		$status = input_post('status');
		$id_post = input_post('id');

		$query_update = "update user_paket up join paket p on p.id=up.id_paket join user u on u.id=up.id_user set up.status='$status',up.waktu_selesai='$waktu_selesai' where up.id_user_paket='$id_post'" ;

		mysqli_query($koneksi, $query_update);
		set_message('msg', 'success', "Edit Pesanan Berhasil");
		redirect('./pesanan.php');
		
	}			
}
?>
<div class="body-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-list-alt fa-fw"></i> Edit Pesanan</h1>
				<div class="row">
					<div class="col-sm-2">
						<div class="list-group">
							<a href="./pesanan.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Pesanan</a>
						</div>
					</div>
					<div class="col-sm-10">
						<form method="post" action="" class="form-horizontal">
							<div class="form-group">
								<label for="nama" class="control-label col-sm-3">Kode Pesanan</label>
								<div class="col-sm-4">
									<input type="text" name="kode_pesanan" id="kode_pesanan" class="form-control" value="<?php echo form_set_value('kode_pesanan', $pesanan['kode_pesanan']); ?>" disabled>
									<?php echo show_error(1, $form_error, 'kode_pesanan'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="alamat" class="control-label col-sm-3">User</label>
								<div class="col-sm-4">
									<input type="text" name="nama_user" id="nama_user" class="form-control" value="<?php echo form_set_value('nama_user', $pesanan['nama_user']); ?>" disabled >
									<?php echo show_error(1, $form_error, 'nama_user'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="telepon" class="control-label col-sm-3">Paket</label>
								<div class="col-sm-4">
									<input type="text" name="paket" id="paket" class="form-control" value="<?php echo form_set_value('paket', $pesanan['paket']); ?>" disabled>
									<?php echo show_error(1, $form_error, 'paket'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="control-label col-sm-3">Jumlah Halaman</label>
								<div class="col-sm-4">
									<input type="text" name="jumlah_halaman" id="jumlah_halaman" class="form-control" value="<?php echo form_set_value('jumlah_halaman', $pesanan['jumlah_halaman']); ?>" disabled>
									<?php echo show_error(1, $form_error, 'jumlah_halaman'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="konfirmasi" class="control-label col-sm-3">Total harga</label>
								<div class="col-sm-4">
									<input type="text" name="total_harga" id="total_harga" class="form-control" value="<?php echo form_set_value('total_harga', rupiah($pesanan['total_harga'])); ?>" disabled>
									<?php echo show_error(1, $form_error, 'total_harga'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="konfirmasi" class="control-label col-sm-3">Waktu Pemesanan</label>
								<div class="col-sm-4">
									<input type="text" name="waktu_pemesanan" id="waktu_pemesanan" class="form-control" value="<?php echo form_set_value('waktu_pemesanan', $pesanan['waktu_pemesanan']); ?>" disabled>
									<?php echo show_error(1, $form_error, 'waktu_pemesanan'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="konfirmasi" class="control-label col-sm-3">Waktu Selesai</label>
								<div class="col-sm-4">
									<input type="text" name="waktu_selesai" id="waktu_selesai" class="form-control" value="<?php echo form_set_value('waktu_selesai', $pesanan['waktu_selesai']); ?>" disabled>
									<?php echo show_error(1, $form_error, 'waktu_selesai'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="status" class="control-label col-sm-3">Status</label>
								<?php
								$data_status = array(
									'proses' => 'Proses',
									'dibayar' => 'Di Bayar',
									'dikirim' => 'Di Kirim',
									'selesai' => 'Selesai'
								);
								?>
								<div class="col-sm-3">
									<select name="status" id="status" class="form-control">
										<?php 
										foreach ($data_status as $key => $value) {
											if($key == $pesanan['status'])
											{
												echo '<option value="'.$key.'" selected="">'.$value.'</option>';
											}
											else
											{
												echo '<option value="'.$key.'">'.$value.'</option>';
											}
										}
										?>										
									</select>										
								</div>
								<input type="hidden" name="id" value="<?php echo $id; ?>" />
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" name="ok" value="simpan" class="btn btn-primary" id="ok"><i class="fa fa-save fa-fw"></i> Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.hapus').click(function()	{
		return confirm('Apakah Anda yakin ingin menghapus data transaksi tersebut?');
	});
</script>
<?php
require_once './footer.php';