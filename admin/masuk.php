<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
if(auth(2, $config['admin_akses']) === true)	{
	redirect('./index.php');
}
if(input_post('masuk'))	{
	$email = input_post('email');
	$password = input_post('password');
	$akses = input_post('akses');
	$is_logged_in = false;
	if(strlen($email) > 0 and strlen($password) > 0)	{
		$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where email='{$email}'"));
		if(count($user) > 0 and encryption(2, $password, $user['password']))	{
			if($user['status'] == 'aktif')	{
				if($user['akses'] == $akses)	{
					session_set('umasuk', true);
					session_set('uid', $user['id']);
					session_set('uakses', $akses);
					session_set('uemail', $user['email']);
					$is_logged_in = true;
				}else{
					set_message('msg', 'danger', 'Anda tidak memiliki hak akses tersebut.');
				}
			}else{
				set_message('msg', 'danger', 'Akun Anda nonaktif, atau belum pernah diaktivasi.');
			}
		}else{
			set_message('msg', 'danger', 'E-mail atau password salah.');			
		}
	}else{
		set_message('msg', 'danger', 'E-mail dan password harus diisi.');
	}
	if($is_logged_in === true)	{
		redirect('./index.php');
	}else{
		redirect('./masuk.php');
	}
}
require_once './header.php';
?>
<div class="body-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div id="login-form">
					<div class="panel panel-default">
						<div class="panel-heading"><i class="fa fa-sign-in fa-fw"></i> Masuk</div>
						<div class="panel-body">
							<?php echo get_message('msg'); ?>
							<form method="post" action="">
								<div class="form-group">
									<label for="email">E-mail</label>
									<input type="text" name="email" id="email" class="form-control">
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<input type="password" name="password" id="password" class="form-control">
								</div>
								<div class="form-group">
									<label for="akses">Akses</label>
									<select name="akses" id="akses" class="form-control">
										<option value="administrator">Administrator</option>
										<option value="operator">Operator</option>
									</select>
								</div>
								<button type="submit" name="masuk" value="masuk" id="masuk" class="btn btn-default"><i class="fa fa-sign-in fa-fw"></i> Masuk</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
require_once './footer.php';