<?php
session_start();
define('PASSED', true);
require_once '../konfigurasi.php';
require_once '../koneksi.php';
require_once '../fungsi.php';
auth(3, $config['admin_akses']);
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-list-alt fa-fw"></i> Pesanan</h1>
				<div class="row">
					<div class="col-sm-2">
						<div class="list-group">
							<a href="./pesanan.php" class="list-group-item"><i class="fa fa-cog fa-fw"></i> Manajemen</a>
						</div>
					</div>
					<div class="col-sm-10">
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode pesanan</th>
									<th>User</th>
									<th>Paket</th>
									<th class="text-right">Jumlah isi</th>
									<th class="text-right">Total harga</th>
									<th class="text-center">Waktu pemesanan</th>
                                    <th class="text"-center">Waktu Selesai</th>
									<th>Status</th>
									<th class="text-center">Opsi</th>
								</tr>
							</thead>				
							<tbody>
								<?php
								$pesanan_query = mysqli_query($koneksi, "select up.*,u.nama as nama_user,p.nama as paket from user_paket up join paket p on p.id=up.id_paket join user u on u.id=up.id_user order by up.status asc, up.waktu_pemesanan asc");
								while($pesanan = mysqli_fetch_assoc($pesanan_query))	{
									echo '<tr>';
									echo '<td>'.$pesanan['kode_pesanan'].'</td>';
									echo '<td>'.$pesanan['nama_user'].'</td>';
									echo '<td>'.$pesanan['paket'].'</td>';
									echo '<td class="text-right">'.$pesanan['jumlah_halaman'].'</td>';
									echo '<td class="text-right">'.rupiah($pesanan['total_harga']).'</td>';
									echo '<td class="text-center">'.$pesanan['waktu_pemesanan'].'</td>';
									echo '<td class="text-center">'.$pesanan['waktu_selesai'].'</td>>';
									echo '<td>'.pesanan_status(2, $pesanan['status']).'</td>';
									echo '<td class="text-center"><a href="edit_transaksi.php?mode=edit&id='.$pesanan['id_user_paket'].'"><i class="fa fa-edit fa-fw"></i></a></td>';
									
									echo '</tr>';
								}
								?>
			
							</tbody>				
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		
<?php	
require_once './footer.php';