<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
if(auth(2, $config['member_akses']) === true)	{
	redirect('./member.php');
}
$aktivasi = input_get('aktivasi');
if(strlen($aktivasi) == 40)	{
	$id = input_get('id');
	$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where id=$id and kode_registrasi='$aktivasi'"));
	if(count($user) > 0)	{
		mysqli_query($koneksi, "update user set status='aktif', kode_registrasi=null where id=$id");
		set_message('msg', 'success', 'Selamat, akun Anda sudah aktif, silahkan masuk.');
		redirect('./masuk.php');
	}else{
		set_message('msg', 'danger', 'Kode aktivasi salah.');
		redirect('./masuk.php');
	}
}
$form_error = null;
if(input_post('daftar'))	{
	$nama = input_post('nama');
	$alamat = input_post('alamat');
	$telepon = input_post('telepon');
	$email = trim(input_post('email'));
	$password = trim(input_post('password'));
	$konfirmasi = trim(input_post('konfirmasi'));
	$akses = 'member';
	$fval = true;
	if(strlen($nama) < 1)	{
		$fval = false;
		$form_error['nama'] = error_generator(1, 'Nama harus diisi.');
	}
	if(strlen($alamat) < 1)	{
		$fval = false;
		$form_error['alamat'] = error_generator(1, 'Alamat harus diisi.');
	}
	if(strlen($telepon) < 1)	{
		$fval = false;
		$form_error['telepon'] = error_generator(1, 'Telepon harus diisi.');
	}
	if(strlen($email) < 1)	{
		$fval = false;
		$form_error['email'] = error_generator(1, 'E-mail harus diisi.');
	}else{
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)	{
			$fval = false;
			$form_error['email'] = error_generator(1, 'Format alamat e-mail tidak valida.');
		}else{
			if(mysqli_num_rows(mysqli_query($koneksi, "select id from user where email='{$email}'")) > 0)	{
				$fval = false;
				$form_error['email'] = error_generator(1, 'Alamat e-mail tersebut sudah digunakan.');
			}
		}
	}
	if(strlen($password) < 8)	{
		$fval = false;
		$form_error['password'] = error_generator(1, 'Password harus diisi, minimal 8 karakter.');
	}else{
		if($password !== $konfirmasi)	{
			$fval = false;
			$form_error['konfirmasi'] = error_generator(1, 'Konfirmasi harus sama dengan password.');
		}
	}
	if($fval === true)	{
		$pass = encryption(1, $password);
		$kode_registrasi = sha1(microtime());
		mysqli_query($koneksi, "insert into user(nama, alamat, telepon, email, password, akses, status, kode_registrasi) values('$nama','$alamat','$telepon','$email','$pass','$akses', 'nonaktif', '$kode_registrasi')");
		$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where email='{$email}'"));
		// Kirim email
		set_message('msg', 'success', "Halo, {$nama}. Terimakasih telah mendaftar menjadi member kami. Kami telah mengirimkan e-mail berisi langkah untuk aktivasi akun Anda. Silahkan dibuka.");
		redirect('./masuk.php');
	}
}
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-wpforms fa-fw"></i> Daftar</h1>
				<form method="post" action="" class="form-horizontal">
					<div class="form-group">
						<label for="nama" class="control-label col-sm-3">Nama</label>
						<div class="col-sm-5">
							<input type="text" name="nama" id="nama" class="form-control" value="<?php echo form_set_value('nama') ?>">
							<?php echo show_error(1, $form_error, 'nama'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="alamat" class="control-label col-sm-3">Alamat</label>
						<div class="col-sm-6">
							<input type="text" name="alamat" id="alamat" class="form-control" value="<?php echo form_set_value('alamat') ?>">
							<?php echo show_error(1, $form_error, 'alamat'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="telepon" class="control-label col-sm-3">Telepon</label>
						<div class="col-sm-3">
							<input type="text" name="telepon" id="telepon" class="form-control" value="<?php echo form_set_value('telepon') ?>">
							<?php echo show_error(1, $form_error, 'telepon'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="control-label col-sm-3">E-mail</label>
						<div class="col-sm-6">
							<input type="text" name="email" id="email" class="form-control" value="<?php echo form_set_value('email') ?>">
							<?php echo show_error(1, $form_error, 'email'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="control-label col-sm-3">Password</label>
						<div class="col-sm-4">
							<input type="password" name="password" id="password" class="form-control">
							<?php echo show_error(1, $form_error, 'password'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="konfirmasi" class="control-label col-sm-3">Konfirmasi</label>
						<div class="col-sm-4">
							<input type="password" name="konfirmasi" id="konfirmasi" class="form-control">
							<?php echo show_error(1, $form_error, 'konfirmasi'); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<button type="submit" name="daftar" value="daftar" id="daftar" class="btn btn-primary"><i class="fa fa-wpforms fa-fw"></i> Daftar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once './footer.php';