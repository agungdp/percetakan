<?php
session_start();
define('PASSED', true);
require_once './konfigurasi.php';
require_once './koneksi.php';
require_once './fungsi.php';
auth(3, $config['member_akses']);
$form_error = null;
if(input_post('ok'))	{
	$password = input_post('password');
	$password_baru = input_post('password_baru');
	$konfirmasi = input_post('konfirmasi');
	$fval = true;
	$user = mysqli_fetch_assoc(mysqli_query($koneksi, "select * from user where id=".session_get('uid')));
	if(encryption(2, $password, $user['password']) === false)	{
		$fval = false;
		$form_error['password'] = error_generator(1, 'Password yang Anda masukkan salah.');
	}
	if(strlen($password_baru) < 8)	{
		$fval = false;
		$form_error['password_baru'] = error_generator(1, 'Password baru minimal 8 karakter.');
	}else{
		if($password_baru !== $konfirmasi)	{
			$fval = false;
			$form_error['konfirmasi'] = error_generator(1, 'Konfirmasi harus sama dengan password baru.');
		}
	}
	if($fval === true)	{
		$pass = encryption(1, $password_baru);
		mysqli_query($koneksi, "update user set password='{$pass}' where id=".session_get('uid'));
		redirect('./keluar.php?mode=ubah_password');
	}
}
require_once './header.php';
require_once './navigasi.php';
?>
<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title1"><i class="fa fa-lock fa-fw"></i> Ubah password</h1>
				<form method="post" action="" class="form-horizontal">
					<div class="form-group">
						<label for="password" class="control-label col-sm-3">Password</label>
						<div class="col-sm-4">
							<input type="password" name="password" id="password" class="form-control">
							<?php echo show_error(1, $form_error, 'password'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="password_baru" class="control-label col-sm-3">Password baru</label>
						<div class="col-sm-4">
							<input type="password" name="password_baru" id="password_baru" class="form-control">
							<?php echo show_error(1, $form_error, 'password_baru'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="konfirmasi" class="control-label col-sm-3">Konfirmasi</label>
						<div class="col-sm-4">
							<input type="password" name="konfirmasi" id="konfirmasi" class="form-control">
							<?php echo show_error(1, $form_error, 'konfirmasi'); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<button type="submit" name="ok" value="ok" id="ok" class="btn btn-danger">
								<i class="fa fa-lock fa-fw"></i> Ubah password
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once './footer.php';