<?php
defined('PASSED') or exit('No direct script access allowed!');
date_default_timezone_set('Asia/Jakarta');
function array_processor($mode, $array)	{
	$result = null;
	switch($mode)	{
		case 1:
			$total_array = count($array);
			$pre = '(';
			foreach ($array as $arr) {
				$pre.=$arr;
				if($total_array>1)	{
					$pre.=',';
				}
				$total_array--;
			}
			$pre.=')';
			$result = $pre;
			break;
	}
	return $result;
}
function auth($mode, $akses = null, $content = null)	{
	$result = false;
	switch($mode)	{
		case 1:
			if(session_get('umasuk') === true)	{
				$result = true;
			}
			break;
		case 2:
			if(auth(1) === true)	{
				if(is_array($akses))	{
					if(in_array(session_get('uakses'), $akses))	{
						$result = true;
					}
				}else{
					if(session_get('uakses') == $akses)	{
						$result = true;
					}
				}
			}
			break;
		case 3:
			if(auth(2, $akses) === false)	{
				redirect('./masuk.php');
			}
			break;
		case 4:
			if(auth(2, $akses) === true)	{
				return $content;
			}
			break;
	}
	return $result;
}
function encryption($mode, $string, $hashed = null)	{
	$result = null;
	switch($mode)	{
		case 1:
			$result = password_hash($string, PASSWORD_DEFAULT, array('cost' => 11));
			break;
		case 2:
			$result = password_verify($string, $hashed);
			break;
	}
	return $result;
}
function error_generator($mode, $message)	{
	$result = null;
	switch($mode)	{
		case 1:
			$result = '<div class="help-block error1">'.$message.'</div>';
			break;
	}
	return $result;
}
function form_set_dropdown($name, $value, $defaultValue = null)	{
	if(!is_null(input_post($name)))	{
		if(input_post($name) == $value)	{
			return "selected";
		}
	}else{
		if($defaultValue === true)	{
			return "selected";
		}
	}
}
function form_set_value($name, $defaultValue = null)	{
	if(!is_null(input_post($name)))	{
		return input_post($name);
	}else{
		return $defaultValue;
	}
}
function get_message($code)	{
	$msg = session_get($code);
	unset_session($code);
	return $msg;
}
function input_get($name)	{
	if(isset($_GET[$name]))	{
		return $_GET[$name];
	}
}
function input_post($name)	{
	if(isset($_POST[$name]))	{
		return $_POST[$name];
	}
}
function pesanan_status($mode, $status)	{
	$result = null;
	switch($mode)	{
		case 2:
			switch($status)	{
				case 'proses':
				$result = '<span class="label label-primary">Proses</span>';
				break;
				case 'dibayar':
				$result = '<span class="label label-warning">dibayar</span>';
				break;
				case 'dikirim':
				$result = '<span class="label label-info">dikirim</span>';
				break;
				case 'selesai':
				$result = '<span class="label label-success">selesai</span>';
				break;
			}
			break;
	}
	return $result;
}
function redirect($url)	{
	header("location:".$url);
	exit;
}
function rupiah($angka)	{
	return 'Rp. ' . number_format($angka, 0 , '' , '.' );
}
function session_get($name)	{
	if(isset($_SESSION[$name]))	{
		return $_SESSION[$name];
	}
}
function session_set($name, $value)	{
	$_SESSION[$name] = $value;
}
function set_message($code, $mode, $message)	{
	session_set($code, '<div class="alert alert-'.$mode.' fade in">'.$message.' <button class="close" type="button" aria-label="Close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button></div>');
}
function show_error($mode, $name, $code)	{
	$result = null;
	switch($mode)	{
		case 1:
			if(isset($name[$code]))	{
				$result =  $name[$code];
			}
			break;
	}
	return $result;
}
function unset_session($name)	{
	unset($_SESSION[$name]);
}
function upload_file($name, $ftypes = array('jpg|jpeg|png|gif'), $target_dir = '../assets/upload/')	{
	if($_FILES[$name]['size'] > 0)	{
		// $target_dir = '../assets/upload/';
		$target_file = $target_dir.basename($_FILES[$name]['name']);
		$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
		$isImage = getimagesize($_FILES[$name]['tmp_name']);
		if(in_array(strtolower($imageFileType), $ftypes))	{
			if(move_uploaded_file($_FILES[$name]['tmp_name'], $target_file))	{
				$data = array('name' => basename($_FILES[$name]['name']));
				return $data;
			}else{
				return false;
			}
		}else{
			return false;
		}
		// if($isImage !== false)	{
		// 	if(move_uploaded_file($_FILES[$name]['tmp_name'], $target_file))	{
		// 		$data = array('name' => basename($_FILES[$name]['name']));
		// 		return $data;
		// 	}else{
		// 		return false;
		// 	}
		// }else{
		// 	return false;
		// }
	}else{
		return false;
	}
}
function user_status($mode, $status)	{
	switch($mode)	{
		case 2:
			if($status == 'nonaktif')	{
				$result = '<span class="label label-danger">Nonaktif</span>';
			}else{
				$result = '<span class="label label-success">Aktif</span>';
			}
			break;
	}
	return $result;
}
function val_compare($comparison, $value)	{
	if($comparison == $value)	{
		return true;
	}else{
		return false;
	}
}